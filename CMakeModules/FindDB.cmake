# - Find db
# Find the native DB headers and libraries.
#
#  DB_INCLUDE_DIRS - where to find db.h, etc.
#  DB_LIBRARIES    - List of libraries when using db.
#  DB_FOUND        - True if db found.
#

find_path(DB_INCLUDE_DIR db_cxx.h PATH_SUFFIXES db5.3)

find_library(DB_LIBRARY NAMES db_cxx)

set(DB_LIBRARIES ${DB_LIBRARY} )
set(DB_INCLUDE_DIRS ${DB_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set DB_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(DB DEFAULT_MSG DB_LIBRARY DB_INCLUDE_DIR )

mark_as_advanced(DB_INCLUDE_DIR DB_LIBRARY )
