/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */




#ifndef LAMMPSEngine_h
#define LAMMPSEngine_h

#include <mpi.h>
#include <stdio.h>
#include <inttypes.h>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>


#include "AbstractEngine.hpp"
#include "LAMMPSSystem.hpp"
#include "lammps/library.h"






template <class System, class Labeler > class LAMMPSEngine : public AbstractEngine<System, Labeler> {
public:

friend class LammpsSystem;
LAMMPSEngine(boost::property_tree::ptree &config, MPI_Comm localComm_, int seed_) : AbstractEngine<System,Labeler>(config,localComm_,seed_) {

	parser.seed(seed_);
	// TODO: how to pass command-line args to LAMMPS at some point?



	//int argc=3;
	//char *lammps_argv[] = {(char *)"parsplice",(char *)"-screen",(char *)"none", };
	int argc=5;
	char *lammps_argv[] = {(char *)"parsplice",(char *)"-screen",(char *)"none",(char *)"-log",(char *)"none"  };
	lammps_open(argc,lammps_argv,localComm_,&lmp);



	// error check on integer sizes, for storage in System
	// NOTE: avoid error check for int sizes?

	int bigint_bytes = lammps_extract_setting(lmp,(char *) "bigint");
	int tagint_bytes = lammps_extract_setting(lmp,(char *) "tagint");
	int imageint_bytes = lammps_extract_setting(lmp,(char *) "imageint");

	if (bigint_bytes != 8)
		error("LAMMPS must be compiled for 8-byte big integers");
	if (tagint_bytes != 4)
		error("LAMMPS must be compiled for 4-byte atom IDs");
	if (imageint_bytes != 4)
		error("LAMMPS must be compiled for 4-byte image flags");


	//read the LAMMPS scripts

	bootstrapScript=config.get<std::string>("ParSplice.LAMMPSEngine.BootstrapScript");
	mdScript=config.get<std::string>("ParSplice.LAMMPSEngine.MDScript");
	minScript=config.get<std::string>("ParSplice.LAMMPSEngine.MinScript");
	writeScript=config.get<std::string>("ParSplice.LAMMPSEngine.WriteScript");
	initScript=config.get<std::string>("ParSplice.LAMMPSEngine.InitScript");
	postInitScript=config.get<std::string>("ParSplice.LAMMPSEngine.PostInitScript");
	velocityInitScript=config.get<std::string>("ParSplice.LAMMPSEngine.VelocityInitScript");

	//bootstrap the driver here
	//this should setup the base LAMMPS parameters (potentials, species, etc.)
	std::vector<std::string> cmdVector=parser.splitLines(bootstrapScript);
	for(int i=0; i<cmdVector.size(); i++) {
		//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
		lammps_command(lmp,(char *) cmdVector[i].c_str());
	}

	//rjz// Get info needed to set regions for the systems
  useSubLattice=config.get<unsigned>("ParSplice.MultiTrajectories.useSubLattice",0);
  if(useSubLattice){
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nxSL",1));
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nySL",1));
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nzSL",1));
    dSLb = config.get<double>("ParSplice.MultiTrajectories.dbufSL",5.0);
    dSLf = config.get<double>("ParSplice.MultiTrajectories.dfixSL",5.0);
    dSLv = config.get<double>("ParSplice.MultiTrajectories.vacSL",5.0);
    setSublatticeRegions();
  }

};


~LAMMPSEngine(){
	lammps_close(lmp);
};

private:
virtual void md_impl(Task<System> &task){
	LAMMPSSystem *sys = &task.systems[0];



	int natomsBegin;
	int natomsEnd;

	do {
		transferSystem(*sys, task.parameters);
		natomsBegin = (int) *((int64_t *)
		                      lammps_extract_global(lmp,(char *) "natoms"));

		//figure out how many steps to do
		double dt=boost::lexical_cast<double>(task.parameters["Timestep"]);
		double time=boost::lexical_cast<double>(task.parameters["BlockTime"]);
		int nsteps = static_cast<int> (time / dt);
		task.parameters["Nsteps"]=boost::str(boost::format("%1%" ) % nsteps );

		//parse the command string
		std::string rawCmd = mdScript; //task.parameters["MDScript"];
		std::string parsedCmd=parser.parse(rawCmd, task.parameters);
		std::vector<std::string> cmdVector=parser.splitLines(parsedCmd);


		//execute the command string
		for(int i=0; i<cmdVector.size(); i++) {
			//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
			lammps_command(lmp,(char *) cmdVector[i].c_str());
		}


		natomsEnd = (int) *((int64_t *)
		                    lammps_extract_global(lmp,(char *) "natoms"));

		if(natomsEnd!=natomsBegin) std::cout<<"ERROR: LAMMPS LOST ATOMS. RESTARTING TASK"<<std::endl;
	} while(natomsBegin != natomsEnd );

	//transfer back
	lammps_gather_atoms(lmp,(char *) "x",1,3,&sys->x[0]);
	lammps_gather_atoms(lmp,(char *) "v",1,3,&sys->v[0]);
	lammps_gather_atoms(lmp,(char *) "image",0,1,&sys->image[0]);



	//update the hash
	identityHash=hashAtomIdentities(*sys);
	coordinatesHash=hashAtoms(*sys);




};


virtual void init_velocities_impl(Task<System> &task){

	LAMMPSSystem *sys = &task.systems[0];

	transferSystem(*sys, task.parameters);

	//parse the command string
	std::string rawCmd = velocityInitScript;//task.parameters["MinScript"];
	std::string parsedCmd=parser.parse(rawCmd, task.parameters);
	std::vector<std::string> cmdVector=parser.splitLines(parsedCmd);

	//execute the command string
	for(int i=0; i<cmdVector.size(); i++) {
		//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
		lammps_command(lmp,(char *) cmdVector[i].c_str());
	}

	//transfer back
	lammps_gather_atoms(lmp,(char *) "x",1,3,&sys->x[0]);
	lammps_gather_atoms(lmp,(char *) "v",1,3,&sys->v[0]);
	lammps_gather_atoms(lmp,(char *) "image",0,1,&sys->image[0]);

	//update the hash
	identityHash=hashAtomIdentities(*sys);
	coordinatesHash=hashAtoms(*sys);
};


virtual void min_impl(Task<System> &task){

	LAMMPSSystem *sys = &task.systems[0];

	transferSystem(*sys, task.parameters);

	//parse the command string
	std::string rawCmd = minScript;//task.parameters["MinScript"];
	std::string parsedCmd=parser.parse(rawCmd, task.parameters);
	std::vector<std::string> cmdVector=parser.splitLines(parsedCmd);

	//execute the command string
	for(int i=0; i<cmdVector.size(); i++) {
		//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
		lammps_command(lmp,(char *) cmdVector[i].c_str());
	}

	//transfer back
	lammps_gather_atoms(lmp,(char *) "x",1,3,&sys->x[0]);
	lammps_gather_atoms(lmp,(char *) "v",1,3,&sys->v[0]);
	lammps_gather_atoms(lmp,(char *) "image",0,1,&sys->image[0]);

	//update the hash
	identityHash=hashAtomIdentities(*sys);
	coordinatesHash=hashAtoms(*sys);
};


virtual void file_init_impl(Task<System> &task){

	if (task.parameters.find("Filename") != task.parameters.end()) {
		std::string configurationFile = task.parameters["Filename"];
		std::cout<<"Filename: "<<configurationFile<<std::endl;

		//parse the command string
		std::string rawCmd = initScript;// task.parameters["InitScript"];
		std::string parsedCmd=parser.parse(rawCmd, task.parameters);
		std::vector<std::string> cmdVector=parser.splitLines(parsedCmd);


		//execute the command string
		for(int i=0; i<cmdVector.size(); i++) {
			//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
			lammps_command(lmp,(char *) cmdVector[i].c_str());
		}
	} else error("LAMMPS file init: unrecognized parameters");


	//initialize the system from the LAMMPS instance

	//set flags based on LAMMPS setup


	triclinic = *((int *) lammps_extract_global(lmp,(char *) "triclinic"));
	qflag = *((int *) lammps_extract_global(lmp,(char *) "q_flag"));
	//mflag = *((int *) lammps_extract_global(lmp,(char *) "molecule_flag"));
	// setup System by querying LAMMPS
	// set initial timestep = 0


	LAMMPSSystem newsys;
	task.systems.push_back(newsys);
	LAMMPSSystem *sys = &task.systems[0];

	int ntimestep = 0;
	sys->setNTimestep(ntimestep);

	sys->qflag = qflag;
	//sys->mflag = mflag;
	int natoms = (int) *((int64_t *)
	                     lammps_extract_global(lmp,(char *) "natoms"));
	sys->setNAtoms(natoms);

	// box info

	double *boxlo = (double *) lammps_extract_global(lmp,(char *) "boxlo");
	double *boxhi = (double *) lammps_extract_global(lmp,(char *) "boxhi");
	memcpy(&sys->boxlo[0],boxlo,NDIM*sizeof(double));
	memcpy(&sys->boxhi[0],boxhi,NDIM*sizeof(double));
	sys->xy = *((double *) lammps_extract_global(lmp,(char *) "xy"));
	sys->xz = *((double *) lammps_extract_global(lmp,(char *) "xz"));
	sys->yz = *((double *) lammps_extract_global(lmp,(char *) "yz"));

	sys->box2abc();

	// NOTE: this is a double within System?
	int *periodicity = (int *) lammps_extract_global(lmp,(char *) "periodicity");
	sys->setPeriodic(0,periodicity[0]);
	sys->setPeriodic(1,periodicity[1]);
	sys->setPeriodic(2,periodicity[2]);

	// per-atom info

	lammps_gather_atoms(lmp,(char *) "id",0,1,&sys->id[0]);
	lammps_gather_atoms(lmp,(char *) "type",0,1,&sys->species[0]);
	lammps_gather_atoms(lmp,(char *) "x",1,3,&sys->x[0]);
	lammps_gather_atoms(lmp,(char *) "v",1,3,&sys->v[0]);
	lammps_gather_atoms(lmp,(char *) "image",0,1,&sys->image[0]);
	if (qflag) lammps_gather_atoms(lmp,(char *) "q",1,1,&sys->q[0]);
	//if (mflag) lammps_gather_atoms(lmp,(char *) "molecule",1,1,&sys->q[0]);
	identityHash=hashAtomIdentities(*sys);
	coordinatesHash=hashAtoms(*sys);

};

virtual void file_write_impl(Task<System> &task) {
	LAMMPSSystem *sys = &task.systems[0];
	transferSystem(*sys, task.parameters);




	if (task.parameters.find("Filename") != task.parameters.end()) {
		std::string file = task.parameters["Filename"];
		file=parser.parse(file, task.parameters);
		task.parameters["Filename"]=file;

		std::cout<<"Filename: "<<file<<std::endl;

		boost::filesystem::path p {file};
		boost::filesystem::path dir=p.parent_path();
		boost::filesystem::create_directories(dir);

		//parse the command string
		std::string rawCmd = writeScript;//task.parameters["WriteScript"];
		std::string parsedCmd=parser.parse(rawCmd, task.parameters);
		std::cout<<parsedCmd<<std::endl;
		std::vector<std::string> cmdVector=parser.splitLines(parsedCmd);


		//execute the command string
		for(int i=0; i<cmdVector.size(); i++) {
			//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
			lammps_command(lmp,(char *) cmdVector[i].c_str());
		}
	} else error("LAMMPS file init: unrecognized parameters");
};

virtual void sync_sl_impl(Task<System> &task){

  std::cout<<" RUNNING SYNC_SL_IMPL. label = "<<task.systems[0].label<<std::endl;

  if(0){ // DEBUG SETTING -> 1
    // EARLY RETURN (DON'T REALLY DO THE SYNCHRONIZATION):
    task.attributes["label"] = boost::lexical_cast<std::string>(task.systems[0].label);
    return;
	}

  // Start with the original atoms:
  LAMMPSSystem newsys = task.systems[0];
  int natoms_new= task.systems[0].getNAtoms();

  std::cout<<" SYNC START REPLACING ATOMS.. natom = "<<natoms_new<<std::endl;

  //std::vector<BoxData> boxes;

  // Define all boxes to use for update..
  // Must apply changes in REVERSE order. [RJZ: This doesnt seem to be what is happeneing???]
  //for(int i=cl.size()-1; i>=0; i--){ //[RJZ: Changing this iteration pattern for now]

  std::vector<int> cli; //changelist item
  // changelist (and cl) is a list of relation vectors
  // Each of these vectors is the direction of a "changed" neighbor
  // Index "ind" of task.changelist corresponds to index "ind+1" of task.systems
  std::list<std::vector<int>> cl = task.changelist;
  for(int ichange=0; ichange<cl.size(); ichange++){

    cli = cl.front(); cl.pop_front();
    BoxData change = BoxData(cli);

    // Define box to "trim" from newsys:
    for(int j=0; j<NDIM; j++){
      if(cli[j] > 0){
        change.xlo[j] = boxhi_own[j]-dSLb-dSLf;
        change.xhi[j] = boxhi_own[j]+dSLf+dSLb;
        // Translation to box location on neighbor:
        change.dshift[j] = -(boxhi_own[j]-boxlo_own[j]);

        // Note that the SLD we are changing will "own" the change.xlo boundary
        // (closest to its center). So, in this case (where change.dshift is NEGATIVE),
        // the SLD will leave the existing atoms ON its change.xlo boundary.
        // However, it will use the neighbor-provided atoms on the change.hi boundary.

      }else if(cli[j] < 0){
        change.xlo[j] = boxlo_own[j]-dSLb-dSLf;
        change.xhi[j] = boxlo_own[j]+dSLf+dSLb;
        // Translation to box location on neighbor:
        change.dshift[j] = (boxhi_own[j]-boxlo_own[j]);

        // Note that the SLD we are changing will "own" the change.xhi boundary
        // (closest to its center). So, in this case (where change.dshift is POSITIVE),
        // the SLD will leave the existing atoms ON its change.xhi boundary.
        // However, it will use the neighbor-provided atoms on the change.lo boundary.

      }else{
        // If the neighbor is aligned in this dimesion,
        // we can take all atoms in this dimension
        change.xlo[j] = task.systems[0].boxlo[j];
        change.xhi[j] = task.systems[0].boxhi[j];
        // Translation to box location on neighbor:
        change.dshift[j] = 0.0;

        // Note that the SLD can just replace its existing atoms with ALL
        // neighbor-provided atoms in this dimension.

      }
    }

    // Now loop through atoms and remove any atom that is in the "box"
    // "belonging" to the current neighbor:
    {
      int i=0;
      while(i < natoms_new){
        bool met = 1;
        for(int j=0; j<NDIM; j++){
          double x0 = newsys.getPosition(i,j);
          //// "Remove" the atom if the following condition is NOT met:
          //// Own xlo, so met=0 if x0 <= change.xlo[j]
          //if((change.dshift[j]>0) && ((x0 <= change.xlo[j]) || (x0 > change.xhi[j]))){
          //  met = 0;
          //// Own xhi, so met=0 if x0 >= change.xhi[j]
				  //}else if((change.dshift[j]<0) && ((x0 < change.xlo[j]) || (x0 >= change.xhi[j]))){
          //  met = 0;
          //}else if((x0 < change.xlo[j]) || (x0 > change.xhi[j])){
          //  met = 0;
          //}
          //if(met==0) break; //go to the "i++" statement and continue the while loop

          // Just remove any exisitng atom in the box (including the boundary)
          // (so DONT remove atoms completely outside the box)
          if((x0 < change.xlo[j]) || (x0 > change.xhi[j])){
            met = 0;
          }

        }
        if(met){
          // Swap this atom with last atom in the list,
          // and then de-increment the atom count (effectively deleting it):
          natoms_new--;
          int speciesEnd = newsys.getSpecies(natoms_new);
          double qEnd = newsys.q[natoms_new];
          for(int jj=0; jj<NDIM; jj++){
            double xEnd  = newsys.getPosition(natoms_new,jj);
            double vEnd  = newsys.getVelocity(natoms_new,jj);
            newsys.setPosition(i,jj,xEnd);
            newsys.setVelocity(i,jj,vEnd);
          }
          newsys.setSpecies(i,speciesEnd);
          newsys.q[i] = qEnd;
        }
        if(!met) i++; // Need to check the swapped atom if the condition WAS met
      }
    }

    // DONE deleting atoms for this neighbor:
    newsys.resize(natoms_new);

    if(1){ // debug if(0)

    // Now loop thrugh atoms in the neighbor, and add the appropriate
    // atoms to newsys...
    LAMMPSSystem neighsys = task.systems[ichange+1];
    for(int i=0; i<neighsys.getNAtoms(); i++){

      bool met = 1;

      for(int j=0; j<NDIM; j++){
        double x0 = neighsys.getPosition(i,j);
        // "Add" the atom if the following condition is met:
        double lowbound = change.xlo[j]+change.dshift[j];
        double highbound = change.xhi[j]+change.dshift[j];
				double ddx=0.0;

        // Just add any atom that is in the box (or on the boundary)
        // So DONt add atoms that are completely outside the box
				if((x0 < lowbound) || (x0 > highbound)){
          met = 0; break;
			  }

				//if((change.dshift[j]==0) && ((x0 < lowbound) || (x0 > highbound))){
        //  met = 0; break;
				//// Own xlo, so met=0 if x0 <= lowbound
        //}else if((change.dshift[j]<0) && ((x0 <= lowbound) || (x0 > highbound))){
        //  ddx = std::abs(x0-lowbound);
				//	//if(ddx>disptol) {met = 0; break;}
				//	if(1) {met = 0; break;}
				//	bdrychk = 1;
        //// Own xhi, so met=0 if x0 >= highbound
			  //}else if((change.dshift[j]>0) && ((x0 < lowbound) || (x0 >= highbound))){
        //  ddx = std::abs(x0-highbound);
				//	//if(ddx>disptol) {met = 0; break;}
				//	if(1) {met = 0; break;}
				//	bdrychk = 1;
        // Neighbor atom is in right place, but near bdry.. lets double check
			  //}else if(change.dshift[j]<0){
	      //    ddx = std::abs(x0-lowbound);
				//		if(ddx<=disptol) bdrychk = 1;
        //// Neighbor atom is in right place, but near bdry.. lets double check
			  //}else if(change.dshift[j]>0){
	      //    ddx = std::abs(x0-highbound);
				//		if(ddx<=disptol) bdrychk = 1;
        //}

      }

      if(met){
        // Add this atom to the end of the newsys list of atoms:
        natoms_new++;
        // NEED TO "RESIZE" IF THE ATOM COUNT IS TOO LARGE:
        if(natoms_new > newsys.getNAtoms()){
          //std::cout<<" SYNC NEEDS RESIZE. natom = "<<natoms_new<<std::endl;
          newsys.resize(natoms_new);
        }
        int speciesAdd = neighsys.getSpecies(i);
        double qAdd = neighsys.q[i];
        for(int jj=0; jj<NDIM; jj++){
          // Need to "shift" the coordinate from the neighbor
          // to the current SLD (trajectory)
          double xAdd  = neighsys.getPosition(i,jj)-change.dshift[jj];
          double vAdd  = neighsys.getVelocity(i,jj);
          newsys.setPosition(natoms_new-1,jj,xAdd);
          newsys.setVelocity(natoms_new-1,jj,vAdd);
        }
        newsys.setSpecies(natoms_new-1,speciesAdd);
        newsys.q[natoms_new-1] = qAdd;
      }
    }

    } //end debug if()

  }

  // Make sure all the atoms have unique IDs:
  for(int i=0; i<newsys.getNAtoms(); i++){
    newsys.setUniqueID(i,i+1);
    newsys.image[i] = 0;
    for(int jj=0; jj<NDIM; jj++){
      newsys.setVelocity(i,jj,0.0);
    }
  }

  std::cout<<" SYNC DONE REPLACING ATOMS.. natom = "<<natoms_new<<std::endl;

  // DEBUG: Write all the new coordinates to stdout:
  if(0){
    for(int i=0; i<newsys.getNAtoms(); i++){
      std::cout<<i+1;
      for(int jj=0; jj<NDIM; jj++){ std::cout<<"\t"<<newsys.getPosition(i,jj); }
      std::cout<<"\t"<<newsys.getUniqueID(i);
      std::cout<<std::endl;
    }
  }

  // Run 'MIN' task
	Task<System> min;
	min.type=PARSPLICE_TASK_MIN;
	min.flavor=task.flavor;
	min.parameters=task.parameters;
	min.systems.clear();
  min.systems.push_back(newsys);
  min.systems[0].type=TYPE::MINIMUM;
  this->process(min);

  std::cout<<" SYNC MIN DONE."<<std::endl;

  // Run 'LABEL' task
  Task<System> label;
  label.type=PARSPLICE_TASK_LABEL;
  label.flavor=task.flavor;
  label.parameters=task.parameters;
	label.systems.clear();
  label.systems.push_back(min.systems[0]);
  this->process(label);

  std::cout<<" SYNC DONE - new label = "<<label.systems[0].label<<" natoms = "<<label.systems[0].getNAtoms()<<std::endl;

  // Record Result:
  task.attributes["label"] = boost::lexical_cast<std::string>(label.systems[0].label);
  task.systems.clear();
  task.systems.push_back(label.systems[0]); // Give the NodeMan a new minimum to "put"

};

virtual void check_sl_impl(Task<System> &task){

  if(not useSubLattice) return;

  disptol=0.2; // Default value (If SEGMENT does not have this parameter...)
  if(task.parameters.find("DispTolSLD") == task.parameters.end()){
	  disptol=boost::lexical_cast<double>(task.parameters["DispTolSLD"]);
	}

  int natoms0 = task.systems[0].getNAtoms();
  int natoms  = task.systems[1].getNAtoms();
  if(natoms0 != natoms){
    std::cout<<"ERROR!! natoms = "<<natoms
             <<", but natoms0 = "<<natoms0<<std::endl;
  }
  std::vector<double> cmxyz = {0.0,0.0,0.0};
  int ntrans = 0;
  std::vector<double> dxhi;
  std::vector<double> dxlo;
  for(int iatom=0; iatom<natoms; iatom++){
    double dx2 = 0.0;
    for(int j=0; j<NDIM; j++){
      double x0 = task.systems[0].getPosition(iatom,j);
      double x1 = task.systems[1].getPosition(iatom,j);
      dx2 += std::pow((x1-x0),2.0);
    }
    double dr = std::sqrt(dx2);
    if(dr >= disptol){
      ntrans += 1;
      for(int j=0; j<NDIM; j++){
        double x0 = task.systems[0].getPosition(iatom,j);
        double x1 = task.systems[1].getPosition(iatom,j);
        cmxyz[j] += task.systems[0].getPosition(iatom,j);
        std::cout<<"ATOM "<<iatom<<" MOVED: x0 = "<<x0<<" x1 = "<<x1
        <<" dr = "<<dr<<" disptol = "<<disptol<<" for dim "<<j<<std::endl;
        if(ntrans==1){
          dxlo.push_back(x0);
          dxhi.push_back(x0);
        }else{
          if(x0>dxhi[j]) dxhi[j] = x0;
          if(x0<dxlo[j]) dxlo[j] = x0;
        }
      }
    }
  }
  if(ntrans > 0){
    double transrad = 0.0;
    for(int j=0; j<NDIM; j++){
      cmxyz[j] = cmxyz[j] / ((double) ntrans);
      if((cmxyz[j] < boxlo_own[j]) || (cmxyz[j] > boxhi_own[j])){
        // This transition belongs to a different SLD...
        // Put system back into the initial state:
        task.systems[1] = task.systems[0];
        std::cout<<"TRANSITION REJECTED BY SLD. cmxyz = "<<cmxyz[j]
                 <<", for dimension = "<<j
                 <<", with LOW bdry = "<<boxlo_own[j]
                 <<", and HI bdry = "<<boxhi_own[j]<<std::endl;
        return;
      }
      transrad += (dxhi[j] - dxlo[j]) * (dxhi[j] - dxlo[j]);
    }
    std::cout<<"TRANSITION ALLOWED BY SLD. cmxyz = "<<cmxyz[0]
             <<" "<<cmxyz[1]<<" "<<cmxyz[2]
             <<", ntrans = "<<ntrans<<std::endl;
    transrad = 0.5*std::sqrt(transrad); // Maximum transition radius
    //task.transcmr = {cmxyz[0], cmxyz[1], cmxyz[2], transrad};
  }else{
    std::cout<<"NO TRANSITION DETECTED IN checksl."<<std::endl;
  }
};

void clean_char(char *var){
  int i = 0;
  while(var[i] != '\0'){
    var[i] = '\0'; i++;
  }
}

void setSublatticeRegions(){

	char cmd[128];
    double xlo0,xhi0,ylo0,yhi0,zlo0,zhi0;
    double xlo1,xhi1,ylo1,yhi1,zlo1,zhi1;
    double *boxlo = (double *) lammps_extract_global(lmp,(char *) "boxlo");
    double *boxhi = (double *) lammps_extract_global(lmp,(char *) "boxhi");

    int n_fixed_regions = 0;
    for(int idim=0; idim<NDIM; idim++){
      if(nxyzSL[idim]>1){
        boxlo_free[idim] = boxlo[idim] + dSLv + dSLf;
        boxhi_free[idim] = boxhi[idim] - dSLv - dSLf;
        boxlo_own[idim]  = boxlo_free[idim] + dSLb;
        boxhi_own[idim]  = boxhi_free[idim] - dSLb;
        if(dSLf > 0.0){
          if(idim == 0){
            xlo0 = boxlo[0];       xlo1 = boxhi_free[0];
            xhi0 = boxlo_free[0];  xhi1 = boxhi[0];
            ylo0 = boxlo[1];       ylo1 = boxlo[1];
            yhi0 = boxhi[1];       yhi1 = boxhi[1];
            zlo0 = boxlo[2];       zlo1 = boxlo[2];
            zhi0 = boxhi[2];       zhi1 = boxhi[2];
          }else if(idim == 1){
            xlo0 = boxlo[0];       xlo1 = boxlo[0];
            xhi0 = boxhi[0];       xhi1 = boxhi[0];
            ylo0 = boxlo[1];       ylo1 = boxhi_free[1];
            yhi0 = boxlo_free[1];  yhi1 = boxhi[1];
            zlo0 = boxlo[2];       zlo1 = boxlo[2];
            zhi0 = boxhi[2];       zhi1 = boxhi[2];
          }else if(idim == 2){
            xlo0 = boxlo[0];       xlo1 = boxlo[0];
            xhi0 = boxhi[0];       xhi1 = boxhi[0];
            ylo0 = boxlo[1];       ylo1 = boxlo[1];
            yhi0 = boxhi[1];       yhi1 = boxhi[1];
            zlo0 = boxlo[2];       zlo1 = boxhi_free[2];
            zhi0 = boxlo_free[2];  zhi1 = boxhi[2];
          }

          n_fixed_regions += 1;
          sprintf(cmd,"region fixreg%d block %g %g %g %g %g %g",
                  n_fixed_regions,xlo0,xhi0,ylo0,yhi0,zlo0,zhi0);
          std::cout<<cmd<<std::endl;
          lammps_command(lmp,cmd); clean_char(&cmd[0]);

          n_fixed_regions += 1;
          sprintf(cmd,"region fixreg%d block %g %g %g %g %g %g",
                  n_fixed_regions,xlo1,xhi1,ylo1,yhi1,zlo1,zhi1);
          std::cout<<cmd<<std::endl;
          lammps_command(lmp,cmd); clean_char(&cmd[0]);

        }
      }else{
        boxlo_free[idim] = boxlo[idim];
        boxhi_free[idim] = boxhi[idim];
        boxlo_own[idim]  = boxlo_free[idim];
        boxhi_own[idim]  = boxhi_free[idim];
      }
    }

    if(n_fixed_regions>0){
      std::vector<std::string> lmpcmd_list;
      lmpcmd_list.push_back("region fixregSL union");
      lmpcmd_list.push_back(boost::lexical_cast<std::string>(n_fixed_regions+1));
      for(int ireg=0; ireg<n_fixed_regions; ireg++){
        sprintf(cmd,"fixreg%d",(ireg+1));
        lmpcmd_list.push_back(cmd); clean_char(&cmd[0]);
      }
      sprintf(cmd,"fixregGL");
      lmpcmd_list.push_back(cmd); clean_char(&cmd[0]);
      std::string lmp_cmd = boost::algorithm::join(lmpcmd_list," ");
      lammps_command(lmp,(char *) lmp_cmd.c_str());
    }

    sprintf(cmd,"region ownregSL block %g %g %g %g %g %g",
            boxlo_own[0],boxhi_own[0],
            boxlo_own[1],boxhi_own[1],
            boxlo_own[2],boxhi_own[2]);
    std::cout<<cmd<<std::endl;
    lammps_command(lmp,cmd); clean_char(&cmd[0]);

    sprintf(cmd,"region freeregSL block %g %g %g %g %g %g",
            boxlo_free[0],boxhi_free[0],
            boxlo_free[1],boxhi_free[1],
            boxlo_free[2],boxhi_free[2]);
    std::cout<<cmd<<std::endl;
    lammps_command(lmp,cmd); clean_char(&cmd[0]);

    sprintf(cmd,"run 0");
    std::cout<<cmd<<std::endl;
    lammps_command(lmp,cmd); clean_char(&cmd[0]);
};

void transferSystem(System &sys, std::map<std::string, std::string> &parameters){
	std::size_t iHash=hashAtomIdentities(sys);
	std::size_t cHash=hashAtoms(sys);

	resetInternalState(sys, parameters);
	return;

	//BUG: this appears to be broken. Disabled for now.
	if(identityHash==iHash and coordinatesHash==cHash) {
		//nothing to do, the LAMMPS is already up to date
		//std::cout<<"LAMMPSEngine<System,Labeler>::transferSystem: System is up to date"<<std::endl;
	}
	else if(identityHash==iHash) {
		//atoms are the same, only positions changed
		//std::cout<<"LAMMPSEngine<System,Labeler>::transferSystem: Coordinates are outdated"<<std::endl;
		updateInternalState(sys, parameters);
	}
	else{
		//brand new system
		//std::cout<<"LAMMPSEngine<System,Labeler>::transferSystem: Different system, full reset"<<std::endl;
		resetInternalState(sys, parameters);
	}
};

void updateInternalState(System &sys, std::map<std::string, std::string> &parameters){
	int natoms = sys.getNAtoms();
	int natoms_lammps = (int) *((int64_t *)
	                            lammps_extract_global(lmp,(char *) "natoms"));
	if (natoms != natoms_lammps) {
		std::cout<<"natoms: "<<natoms<<" natoms_lammps: "<<natoms_lammps<<std::endl;
		error("LAMMPS MD run starting with inconsistent atom count");
	}

	// just reset x,v,image
	// assume id and type have not changed
	// NOTE: scatter requires that atom map exists

	lammps_scatter_atoms(lmp,(char *) "x",1,3,&sys.x[0]);
	lammps_scatter_atoms(lmp,(char *) "v",1,3,&sys.v[0]);
	lammps_scatter_atoms(lmp,(char *) "image",0,1,&sys.image[0]);

	//update the hashes
	identityHash=hashAtomIdentities(sys);
	coordinatesHash=hashAtoms(sys);

};


void resetInternalState(System &sys, std::map<std::string, std::string> &parameters){

	int natoms = sys.getNAtoms();
	char cmd[512];
	lammps_command(lmp,(char *) "delete_atoms group all");

	sys.abc2box();
	bool triclinic = *((int *) lammps_extract_global(lmp,(char *) "triclinic"));
	if (triclinic)
		sprintf(cmd,"change_box all "
		        "x final %g %g y final %g %g z final %g %g "
		        "xy final %g xz final %g yz final %g units box",
		        sys.boxlo[0],sys.boxhi[0],
		        sys.boxlo[1],sys.boxhi[1],
		        sys.boxlo[2],sys.boxhi[2],
		        sys.xy,sys.xz,sys.yz);
	else
		sprintf(cmd,"change_box all "
		        "x final %g %g y final %g %g z final %g %g units box",
		        sys.boxlo[0],sys.boxhi[0],
		        sys.boxlo[1],sys.boxhi[1],
		        sys.boxlo[2],sys.boxhi[2]);
	lammps_command(lmp,cmd);

	// NOTE: should I also (re)set periodicity?   or check has not changed?
	// NOTE: periodicity is a double within System?

	// NOTE: what about remapping of atom back into box with image change?

	lammps_create_atoms(lmp,natoms,
	                    &sys.id[0],&sys.species[0],&sys.x[0],&sys.v[0],NULL,1);
	lammps_scatter_atoms(lmp,(char *) "image",0,1,&sys.image[0]);
	if (sys.qflag) lammps_scatter_atoms(lmp,(char *) "q",0,1,&sys.q[0]);
	//if (sys.mflag) lammps_scatter_atoms(lmp,(char *) "molecule",0,1,&sys.molecule[0]);

	//parse the command string
	std::string rawCmd = postInitScript; //parameters["PostInitScript"];
	std::string parsedCmd=parser.parse(rawCmd, parameters);
	std::vector<std::string> cmdVector=parser.splitLines(parsedCmd);

	//execute the command string
	for(int i=0; i<cmdVector.size(); i++) {
		//std::cout<<"EXECUTING "<<cmdVector[i]<<std::endl;
		lammps_command(lmp,(char *) cmdVector[i].c_str());
	}

	//update the hashes
	identityHash=hashAtomIdentities(sys);
	coordinatesHash=hashAtoms(sys);

};

std::size_t hashAtomIdentities(System &s){
	std::size_t hash = 0;
	boost::hash_combine(hash, s.id);
	boost::hash_combine(hash, s.species);
	boost::hash_combine(hash, s.q);
	boost::hash_combine(hash, s.boxabc);
	boost::hash_combine(hash, s.periodicity);

	return hash;
};
std::size_t hashAtoms(System &s){
	std::size_t hash = hashAtomIdentities(s);
	boost::hash_combine(hash, s.x);
	boost::hash_combine(hash, s.v);
	return hash;
};

// LAMMPS specific variables
int me,nprocs;               // MPI info
void *lmp;                   // instance of LAMMPS
int triclinic;               // 1/0 for triclinic/orthogonal simulation box
int qflag;                   // 1/0 for yes/no charges defined
int mflag;
void error(const char *str){

	if (me == 0) printf("ERROR: %s\n",str);
	MPI_Abort(MPI_COMM_WORLD,1);

};

private:
ParameterParser parser;
std::size_t identityHash;
std::size_t coordinatesHash;

//rjz// Sublattice info (for setting regions)
bool useSubLattice;
double disptol;
std::vector<int> nxyzSL;
double dSLb;
double dSLf;
double dSLv;
std::array<double, NDIM> boxlo_own;
std::array<double, NDIM> boxhi_own;
std::array<double, NDIM> boxlo_free;
std::array<double, NDIM> boxhi_free;

std::string bootstrapScript;
std::string mdScript;
std::string minScript;
std::string writeScript;
std::string initScript;
std::string postInitScript;
std::string velocityInitScript;
};



#endif /* LAMMPSEngine_h */
