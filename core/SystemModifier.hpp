#ifndef SystemModifier_hpp
#define SystemModifier_hpp


#include <cstdint>
#include <limits>
#include <boost/property_tree/ptree.hpp>
#include <Eigen/Dense>
#include <boost/random/random_device.hpp>
#include <boost/random.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

#include "AbstractSystem.hpp"
#include "NeighborList.hpp"
#include "BoundaryConditions.hpp"

using Eigen::MatrixXd;


template <class System> class AbstractSystemModifier {
public:
AbstractSystemModifier(boost::property_tree::ptree &config){
	timeDelay=config.get<int>("ParSplice.SystemModifier.Delay",0);
};

virtual bool modificationNeeded (uint64_t previousTime, uint64_t currentTime){
	if(timeDelay==0) {
		return false;
	}
	return previousTime/timeDelay != currentTime/timeDelay;
};

virtual bool modify ( System &s, uint64_t previousTime, uint64_t currentTime)=0;

protected:
uint64_t timeDelay;
};


template <class System> class NullSystemModifier : public AbstractSystemModifier<System> {
public:
NullSystemModifier(boost::property_tree::ptree &config) : AbstractSystemModifier<System>(config) {
	//AbstractSystemModifier<System>::AbstractSystemModifier(config);
};
virtual bool modify ( System &s, uint64_t previousTime, uint64_t currentTime){
	return false;
};
};

template <class System> class AtomInserterSystemModifier : public AbstractSystemModifier<System>  {
public:

AtomInserterSystemModifier(boost::property_tree::ptree &config) : AbstractSystemModifier<System>(config){
	boost::random::random_device rd;
	rng.seed(rd());

	x0=std::vector<double>(NDIM,0);
	rcreate=config.get<double>("ParSplice.SystemModifier.RSphere");
	std::string scenter=config.get<std::string>("ParSplice.SystemModifier.Center");
	std::stringstream ss;
	ss<<scenter;
	ss>>x0[0]>>x0[1]>>x0[2];


	BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.SystemModifier.Atoms")) {
		int sp=v.second.get<int>("Label");
		double fraction=v.second.get<double>("Fraction");
		double rclose=v.second.get<double>("RClose");
		double rfar=v.second.get<double>("RFar");

		insertionRanges[sp]=std::make_pair(rclose,rfar);
		fractions[sp]=fraction;

		BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("Attributes")) {
			std::string key=vv.first;
			std::string data=vv.second.data();
			boost::trim(key);
			boost::trim(data);
			//std::cout<<key<<" "<<data<<std::endl;
			attributes[sp][key]=data;
		}
	}
};


virtual bool modifyv ( System &s, uint64_t previousTime, uint64_t currentTime){
	std::cout<<"AtomInserterSystemModifier::modify "<<previousTime<<" "<<currentTime<<std::endl;

	int nInsertions= currentTime/this->timeDelay -previousTime/this->timeDelay;
	//int nInsertions=1;

	std::cout<<"AtomInserterSystemModifier::modify "<<nInsertions<<std::endl;

	if(nInsertions==0) {
		return false;
	}

	Cell bc;
	//compute the projectors
	MatrixXd rspe(NDIM,NDIM);
	MatrixXd cspe(NDIM,NDIM);

	for(int i=0; i<NDIM; i++) {
		bc.periodic[i]=s.getPeriodic(i);
		for(int j=0; j<NDIM; j++) {
			bc.rsp[i][j]=s.getBox(i,j);
			rspe(i,j)=s.getBox(i,j);
		}
	}
	cspe=rspe.inverse();
	for(int i=0; i<NDIM; i++) {
		for(int j=0; j<NDIM; j++) {
			bc.csp[i][j]=cspe(i,j);
		}
	}

	boost::random::uniform_01<> uniform;



	for(int n=0; n<nInsertions; n++) {
		std::cout<<"AtomInserterSystemModifier::modify Inserting"<<std::endl;

		int nAtoms=s.getNAtoms();
		double rs=uniform(rng);
		auto it=fractions.begin();
		double ss=it->second;

		while(ss<rs) {
			it++;
			ss+=it->second;
		}
		int sp=it->first;

		bool inRange;
		double rmin;
		std::vector<double> xs(NDIM,0);
		std::vector<double> xr(NDIM,0);
		//this is slow and dumb. We can do better if needed

		do {
			inRange=false;
			rmin=std::numeric_limits<double>::max();


			//sample a point uniformly on a sphere
			double u=(uniform(rng)-1)*2;
			double theta=uniform(rng)*2*3.14159;

			xr[0]=x0[0]+sqrt(1-u*u)*cos(theta)*rcreate;
			xr[1]=x0[1]+sqrt(1-u*u)*sin(theta)*rcreate;
			xr[2]=u*rcreate;

			//compute min
			for(int i=0; i<nAtoms; i++) {
				double r=nearestImageDistance(xr, i, s, bc);
				rmin=std::min(rmin,r);
			}
			//std::cout<<"RMIN: "<<rmin<<" "<<insertionRanges[sp].first<<" "<<insertionRanges[sp].second<<std::endl;

			//the atoms is not close to anybody
			if( rmin> insertionRanges[sp].second) {

				//pick a random direction
				std::vector<double> dr(NDIM,0);
				std::vector<double> x(NDIM,0);
				double norm=1;
				for(int k=0; k<NDIM; k++) {
					dr[k]=2*(uniform(rng)-0.5);
					norm+=dr[k]*dr[k];
				}
				norm=sqrt(norm);
				for(int k=0; k<NDIM; k++) {
					dr[k]/=norm;
				}

				//move along the random direction until we get in the insertion zone
				for(int itry=0; itry<100; itry++) {
					for(int k=0; k<NDIM; k++) {
						x[k]=xr[k]+dr[k]*itry*(insertionRanges[sp].second-insertionRanges[sp].first)/3;
					}
					rmin=std::numeric_limits<double>::max();
					for(int i=0; i<nAtoms; i++) {
						double r=nearestImageDistance(xr, i, s, bc);
						rmin=std::min(rmin,r);
					}

					if(rmin>insertionRanges[sp].first and rmin<insertionRanges[sp].second) {
						inRange=true;
						s.addAtom(xr,sp,attributes[sp]);
						std::cout<<"INSERTION ACCEPTED"<<std::endl;
						break;
					}
				}
			}
			else{
				std::cerr<<"AtomInserterSystemModifier::modify: SPHERE RADIUS IS TOO SMALL"<<std::endl;
			}
			if(inRange) {
				break;
			}
		}
		while(!inRange);

	}


	std::cout<<"AtomInserterSystemModifier::modify DONE "<<std::endl;

	return true;
};



virtual bool modify ( System &s, uint64_t previousTime, uint64_t currentTime){
	std::cout<<"AtomInserterSystemModifier::modify "<<previousTime<<" "<<currentTime<<std::endl;
	/*
	   if( (not this->modificationNeeded(previousTime,currentTime)) or fractions.size()==0) {
	        return false;
	   }
	 */
	int nInsertions= currentTime/this->timeDelay -previousTime/this->timeDelay;
	//int nInsertions=1;

	std::cout<<"AtomInserterSystemModifier::modify "<<nInsertions<<std::endl;

	if(nInsertions==0) {
		return false;
	}

	Cell bc;
	//compute the projectors
	MatrixXd rspe(NDIM,NDIM);
	MatrixXd cspe(NDIM,NDIM);

	for(int i=0; i<NDIM; i++) {
		bc.periodic[i]=s.getPeriodic(i);
		for(int j=0; j<NDIM; j++) {
			bc.rsp[i][j]=s.getBox(i,j);
			rspe(i,j)=s.getBox(i,j);
		}
	}
	cspe=rspe.inverse();
	for(int i=0; i<NDIM; i++) {
		for(int j=0; j<NDIM; j++) {
			bc.csp[i][j]=cspe(i,j);
		}
	}

	boost::random::uniform_01<> uniform;



	for(int n=0; n<nInsertions; n++) {
		std::cout<<"AtomInserterSystemModifier::modify Inserting"<<std::endl;

		int nAtoms=s.getNAtoms();
		double rs=uniform(rng);
		auto it=fractions.begin();
		double ss=it->second;

		while(ss<rs) {
			it++;
			ss+=it->second;
		}
		int sp=it->first;

		bool inRange;
		double rmin;
		std::vector<double> xs(NDIM,0);
		std::vector<double> xr(NDIM,0);

		//this is slow and dumb. We can do better if needed

		//randomly try to insert until atom is within range
		do {
			inRange=false;
			rmin=1e15;
			//generate a random point in cell space
			for(int k=0; k<NDIM; k++) {
				xs[k]=uniform(rng);
			}
			//project to cell space
			for(int k=0; k<NDIM; k++) {
				xr[k]=0;
				for(int l=0; l<NDIM; l++) {
					xr[k]+=bc.rsp[k][l]*xs[l];
				}
			}
			//std::cout<<xr[0]<<" "<<xr[1]<<" "<<xr[2]<<std::endl;
			//compute min and max distances
			for(int i=0; i<nAtoms; i++) {
				double r=nearestImageDistance(xr, i, s, bc);
				rmin=std::min(rmin,r);
			}
			//std::cout<<"RMIN: "<<rmin<<" "<<insertionRanges[sp].first<<" "<<insertionRanges[sp].second<<std::endl;
			if(rmin>insertionRanges[sp].first and rmin<insertionRanges[sp].second) {
				inRange=true;
				s.addAtom(xr,sp,attributes[sp]);
				std::cout<<"INSERTION ACCEPTED"<<std::endl;
			}

		}
		while(!inRange);

	}


	std::cout<<"AtomInserterSystemModifier::modify DONE "<<std::endl;

	return true;
};

private:

double rcreate;
std::vector<double> x0;
std::map<int,double> fractions;
std::map<int,std::pair<double,double> > insertionRanges;
std::map<int,std::map<std::string,std::string> > attributes;
boost::random::mt11213b rng;
};

#endif
