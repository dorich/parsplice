/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#ifndef NodeManager_h
#define NodeManager_h

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <set>
#include <unordered_set>
#include <chrono>
#include <string>
#include <exception>
#include <iostream>

#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/serialization/list.hpp>
#include <boost/functional/hash/hash.hpp>
#include <boost/filesystem.hpp>


#include <mpi.h>

#include "CustomTypes.hpp"
#include "AbstractSystem.hpp"
#include "Task.hpp"
#include "TaskManager.hpp"
#include "LocalStore.hpp"
#include "DDS.hpp"
#include "Types.hpp"
#include "Constants.hpp"
#include "Validator.hpp"
#include "Pack.hpp"





#define STATE_DBKEY 0


class WorkManager {

public:

WorkManager(MPI_Comm comm_, int parent_, AbstractDDS *minimaStore_, AbstractLocalDataStore *qsdStore_, boost::property_tree::ptree &config, std::set<MPI_Comm> workerComms){
	comm=comm_;
	parent=parent_;
	MPI_Comm_rank(comm,&rank);
	die=false;
	everybodyIsDead=false;
	//number of workers that this manager is in charge of
	//nWorkers=config.get<int>("ParSplice.Topology.WorkersPerManager",1);



	//number of ranks that should be assigned to each worker
	//nRanksDriver=config.get<int>("ParSplice.Topology.RanksPerWorker",1);
	//int nSocketsNode=config.get<int>("ParSplice.Topology.SocketsPerNode",1);
	//bool singleNodeWorker=config.get<bool>("ParSplice.Topology.IntranodeWorkers",true);


	//assert(singleNodeWorker);

	//maximum size of a PARSPLICE_TASK_TAG message
	validatorTimeout=config.get<unsigned>("ParSplice.Splicer.ValidatorTimeout",1000000);
	rbufSize=config.get<int>("ParSplice.MaximumTaskMesgSize",1000000);

	int validationBatchSize=config.get<int>("ParSplice.ValidationBatchSize",1);

	minimaStore=minimaStore_;
	qsdStore=qsdStore_;




	//read task parameters
	BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.TaskParameters")) {
		std::string stype=v.second.get<std::string>("Type");
		boost::trim(stype);
		int type=taskTypeByLabel.at(stype);
		int flavor=v.second.get<int>("Flavor");
		BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("")) {
			std::string key=vv.first;
			std::string data=vv.second.data();
			boost::trim(key);
			boost::trim(data);
			std::cout<<key<<" "<<data<<std::endl;
			parameters[std::make_pair(type,flavor)][key]=data;
		}
	}

	std::set<int> clients;

	for(auto it=workerComms.begin(); it!=workerComms.end(); it++) {
		workers.push_back( DriverHandleType( *it ) );
		tasks.push_back(TaskType());
		clients.insert(workers.size()-1);
	}

	nWorkers=workers.size();

	std::cout<<"N WORKERS: "<<nWorkers<<std::endl;

	//init the validator
	validator.init(clients,validationBatchSize);

	//initialize communications
	recvCompleted=0;
	sendCompleted=1;

	rbuf=std::vector<char>(rbufSize,' ');
	//post a non-blocking recv for new tasks
	MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,PARSPLICE_TASK_TAG,comm,&incomingTaskRequest);

};




~WorkManager(){
};


/**
 * Manager server
 *
 * Manager will directly:
 * -receive tasks from parent
 * -send segments to parent
 * -send task requests to workers
 * -receive completed tasks from workers
 *
 * Node manager will indirectly:
 * -request states to the database
 * -receive states from the database
 */
void server(){


	while(true) {
		//process non-blocking receives
		processRecv();

		//process non-blocking sends
		processSend();

		//release reservations that are no longer needed
		releaseStaleReservations();

		//promote prefetches to ready state
		promoteTasks();

		//process completed tasks
		processCompletedTasks();

		//assign new tasks to idle workers
		assignTasks();

		//update the db's
		updateStores();

		//std::cout<<"L: "<<everybodyIsDead<<std::endl;

		if(everybodyIsDead) {
			std::cout<<"WM OUT"<<std::endl;
			break;
		}
	}
};


void processSend(){

	if(die) {
		//do not send anything more if you were instructed to die
		if(not sendCompleted) {
			MPI_Cancel(&outgoingRequest);
			MPI_Wait(&outgoingRequest,&sendStatus);
			sendCompleted=true;
			std::cout<<"WORK MANAGER: processSend CANCELLED PENDING COMMUNICATIONS"<<std::endl;
		}
	}
	else{
		//test for send completion
		if(not sendCompleted) {
			//std::cout<<"WORK MANAGER TESTING FOR SEND"<<std::endl;
			MPI_Test(&outgoingRequest,&sendCompleted,&sendStatus);
			//std::cout<<"WORK MANAGER DONE TESTING FOR SEND "<<sendCompleted<<std::endl;
		}

		//TODO use a timer to avoid sending too often
		if(sendCompleted) {
			Timer t;

			sbuf.clear();
			bool sendingsync = 0;
			if(syncstatistics.size()>0) {// NEED TO MAKE SURE WE ARE ONLY TRYING TO SEND "syncstatistics" HERE
				std::cout<<"NODE MANAGER: PACKING AND SENDING SYNCSTATISTICS... "<<std::endl;
				pack(sbuf,syncstatistics);
				syncstatistics.clear();
				int count=sbuf.size();
				MPI_Issend( &(sbuf[0]),count,MPI_BYTE,parent,PARSPLICE_SYNC_STATS_TAG, comm, &outgoingRequest);
				sendingsync = 1;
				sendCompleted=0;
				std::cout<<"NODE MANAGER: DONE SENDING SYNCSTATISTICS "<<t.stop()<<std::endl;
			}

			if(not sendingsync) {

				std::list<SegmentDatabase> batches;
				while(true) {
					validator.refreshHead(validatorTimeout);
					SegmentDatabase segments=validator.release();
					if(segments.size()>0) {
						batches.push_back(segments);
					}
					else{
						break;
					}
				}

				//make sure we have something to send
				if(batches.size() + statistics.size() > 0) {

					std::cout<<"SENDING "<<batches.size()<<" BATCHES AND "
					         <<statistics.size()
					         <<" STATISTICS UPDATE "<<std::endl;

					//pack statistics and validated segments
					sbuf.clear();

					//TODO: Fix me to avoid overflows
					pack(sbuf,statistics,batches);
					statistics.clear();
					int count=sbuf.size();
					MPI_Issend( &(sbuf[0]),count,MPI_BYTE,parent,PARSPLICE_STATS_SEGMENTS_TAG, comm, &outgoingRequest);

					sendCompleted=0;
					//std::cout<<"NODE MANAGER: DONE SENDING "<<t.stop()<<std::endl;
				}
			}
		}
	}
};

/**
 *
 */
void processRecv(){

	//if we were told to die, we won't receive any more messages
	if(die) {

		//if(not recvCompleted) {
		//	MPI_Cancel(&incomingTaskRequest);
		//	MPI_Wait(&incomingTaskRequest,&recvStatus);
		//}
		//std::cout<<"WM IN THE PROCESS OF DYING. processRecv NOT RECEIVING MESSAGES"<<std::endl;
	}
	else {
		//did we receive new tasks?
		if(not recvCompleted) {
			//std::cout<<"WORK MANAGER TESTING FOR RECV"<<std::endl;
			MPI_Test(&incomingTaskRequest,&recvCompleted,&recvStatus);
			//std::cout<<"WORK MANAGER DONE TESTING FOR RECV "<<recvCompleted<<std::endl;
		}
		if(recvCompleted) {
			Timer t;


			//extract
			int count=0;
			MPI_Get_count (&recvStatus, MPI_BYTE, &count);
			std::cout<<"WORK MANAGER "<<rank<<" RECEIVED TASKS "<<count<<std::endl;


			{
				//test
				//std::cout<<"HASH R "<<boost::hash_range(rbuf.begin(),rbuf.begin()+count)<<" "<<count<<std::endl;
			}

			TaskDescriptorBundle incomingTasks;
			unpack(rbuf,incomingTasks,std::size_t(count));




			for(auto it=incomingTasks.begin(); it!=incomingTasks.end(); it++) {
				//std::cout<<"TYPE: "<<it->type<<" FLAVOR: "<<it->flavor<<" N INSTANCES "<<it->nInstances<<" IN STATE "<<it->systems[0].label<<" "<<it->batchId<<std::endl;
				if(it->type==PARSPLICE_TASK_DIE) {
					die=true;
					std::cout<<"WORK MANAGER WAS TOLD TO DIE. SENDING DIE MESSAGES"<<std::endl;
				}
			}

			if(not die) {
				//post a new receive for tasks
				MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,PARSPLICE_TASK_TAG,comm,&incomingTaskRequest);
				recvCompleted=0;
			}

			//issue reservations for the states we need to fulfill these tasks
			std::unordered_set<SystemPlaceholder>  requiredSystems;
			incomingTasks.requiredSystems(requiredSystems);
			for(auto it=requiredSystems.begin(); it!=requiredSystems.end(); it++) {
				//we don't yet deal with requirement on non-minimized configurations
				assert(it->type==TYPE::MINIMUM);
				if(it->type==TYPE::MINIMUM and activeReservations.count(std::make_pair(STATE_DBKEY,it->label))==0) {
					std::cout<<"WORK MANAGER "<<rank<<" ISSUING RESERVATION FOR MINIMA "<<it->label<<std::endl;
					activeReservations[std::make_pair(STATE_DBKEY,it->label)]=minimaStore->reserve(STATE_DBKEY,it->label);
				}
			}
			//store the TaskDescriptors in the prefetch list
			prefetches.insert(incomingTasks);


			//std::cout<<"NODE MANAGER: DONE RECEIVING "<<t.stop()<<std::endl;
		}
	}
};


/**
 * Release stale reservations
 */
void releaseStaleReservations(){

	Timer t;
	std::unordered_set<SystemPlaceholder> required;
	prefetches.requiredSystems(required);
	segmentQueue.requiredSystems(required);
	syncQueue.requiredSystems(required);


	for(auto it=activeReservations.begin(); it!=activeReservations.end(); ) {
		SystemPlaceholder s;
		s.type=TYPE::MINIMUM;
		s.necessity=NECESSITY::REQUIRED;
		s.label=it->first.second;
		if(required.count(s)==0) {
			std::cout<<"WORK MANAGER "<<rank<<" RELEASING RESERVATION ON STATE "<<s.label<<std::endl;
			minimaStore->release(it->second);
			it=activeReservations.erase(it);
		}
		else{
			it++;
		}
	}
	//std::cout<<"NODE MANAGER: DONE RELEASING: "<<t.stop()<<std::endl;

};




/**
 * Promote prefetches to ready state
 */
void promoteTasks(){

	//std::cout<<"NODE MANAGER: ABOUT TO START PROMOTING: "<<std::endl;

	Timer t;
	//loop on pending prefetches
	for(auto it=prefetches.tasks.begin(); it!=prefetches.tasks.end(); ) {
		std::list<SystemPlaceholder> deps;
		it->dependencies(deps);
		bool ready=true;
		for(auto itd=deps.begin(); itd!=deps.end(); itd++) {
			if(itd->type==TYPE::MINIMUM) {
				ready=ready and (minimaStore->count(STATE_DBKEY,itd->label) > 0);
			}
			if(itd->type==TYPE::QSD) {
				ready=ready and (qsdStore->count(STATE_DBKEY,itd->label)  > 0);
			}
			if(not ready) {
				break;
			}
		}
		//std::cout<<"NODE MANAGER: PROMOTING: "<<std::endl;
		if(ready) {
			//we only deal with segments at this point
			//assert(it->type==PARSPLICE_TASK_SEGMENT or it->type==PARSPLICE_TASK_SYNC_SL or it->type==PARSPLICE_TASK_DIE);
			if(it->systems.size()>0) {
				std::cout<<"WORK MANAGER "<<rank<<" PROMOTED PREFETCH "<<it->systems[0].label<<std::endl;
			}
			if(it->type==PARSPLICE_TASK_SYNC_SL) syncQueue.insert(*it);
			else segmentQueue.insert(*it);
			it=prefetches.tasks.erase(it);
		}
		else{
			it++;
		}
	}

	//remove stale tasks from the segment queue
	segmentQueue.purge();

	//std::cout<<"NODE MANAGER: DONE PROMOTING: "<<t.stop()<<std::endl;

};


void assignTasks(){
	//std::cout<<"SYNC QUEUE: "<<syncQueue.size()<<std::endl;
	//std::cout<<"SEG QUEUE: "<<segmentQueue.size()<<std::endl;
	bool anyidle = false;
	Timer t;
	everybodyIsDead=true;
	for(int i=0; i<nWorkers; i++) {

		if(workers[i].isIdle() and not workers[i].isDead()) {
			TaskDescriptor td;
			anyidle=true;
			if(syncQueue.next(td) || segmentQueue.sample(td)) {
				std::cout<<"PROMOTED TASK OF TYPE: "<<td.type<<" ("<<syncQueue.size()<<")"<<std::endl;
				tasks[i]=promoteTaskDescriptor(td);
				//std::cout<<tasks[i].type<<" "<<tasks[i].flavor<<" "<<tasks[i].systems[0].label<<std::endl;
				workers[i].assign(tasks[i]);
				if(td.type==PARSPLICE_TASK_DIE) {
					workers[i].die();
					std::cout<<"KILLED WORKER "<<i<<" "<<workers[i].isDead()<<std::endl;
				}
				//std::cout<<"WORK MANAGER "<<rank<<" ASSIGNED TASK TO WORKER "<<i<<std::endl;
			}
		}
		//std::cout<< workers[i].isDead()<<std::endl;
		everybodyIsDead = everybodyIsDead and workers[i].isDead();
	}
	//std::cout<<"NODE MANAGER: DONE ASSIGNING: "<<t.stop()<<" "<<everybodyIsDead<<std::endl;
}

void extractSystems(TaskType &t){
	//std::cout<<"WORK MANAGER "<<rank<<" EXTRACTING SYSTEMS "<<std::endl;

	for(auto it=t.systems.begin(); it!=t.systems.end(); it++) {
		uint64_t label=it->label;
		Rd data;
		if(it->type==TYPE::MINIMUM) {
			data.clear();
			pack(data,*it);
			minimaStore->put(STATE_DBKEY,label,data);
			//std::cout<<"WORK MANAGER "<<rank<<" EXTRACTING MINIMUM "<<label<<std::endl;
		}
		if(it->type==TYPE::QSD) {
			data.clear();
			pack(data,*it);
			qsdStore->put(STATE_DBKEY,label,data);
			//std::cout<<"WORK MANAGER "<<rank<<" EXTRACTING QSD "<<label<<std::endl;
		}
	}
};

void populateSystems(TaskDescriptor &td, TaskType &t){
	t.systems.clear();

	//loop over systems in the task descriptor
	for(auto it=td.systems.begin(); it!=td.systems.end(); it++) {
		SystemType sys;
		Rd data;
		//populate minima
		if(it->type==TYPE::MINIMUM) {
			if(minimaStore->count(STATE_DBKEY,it->label)>0) {
				minimaStore->get(STATE_DBKEY,it->label,data);
				//std::cout<<"WORK MANAGER "<<rank<<" POPULATING MINIMUM "<<it->label<<" "<<data.size()<<std::endl;
				unpack(data,sys,0);
				sys.type=TYPE::MINIMUM;
			}
			else{
				sys.type=TYPE::EMPTY;
			}
		}
		//populate hot points
		if(it->type==TYPE::QSD) {
			if(qsdStore->count(STATE_DBKEY,it->label)>0) {
				qsdStore->get(STATE_DBKEY,it->label,data);
				//std::cout<<"WORK MANAGER "<<rank<<" POPULATING QSD "<<it->label<<" "<<data.size()<<std::endl;
				unpack(data,sys,0);
				sys.type=TYPE::QSD;
			}
			else{
				sys.type=TYPE::EMPTY;
			}
		}
		t.systems.push_back(sys);
	}
}

TaskType promoteTaskDescriptor(TaskDescriptor &td){
	TaskType t(td);
	if(t.type==PARSPLICE_TASK_SYNC_SL) {
		t.batchId = td.batchId;
		t.itrajectory = td.itrajectory;
		t.changelist = td.changelist;
		std::cout<<"sync PROMOTING t.batchId "<<t.batchId<<std::endl;
		std::cout<<"sync PROMOTING t.itrajectory "<<t.itrajectory<<std::endl;
		// PARSPLICE_TASK_SYNC_SL will use the SEGMENT paramters for now
		t.parameters=parameters[std::make_pair(PARSPLICE_TASK_SEGMENT,t.flavor)];
	}else{
		t.parameters=parameters[std::make_pair(t.type,t.flavor)];
	}
	populateSystems(td,t);
	return t;
};

void processCompletedTasks(){
	Timer t;

	for(int i=0; i<nWorkers; i++) {
		if(workers[i].probe(tasks[i])) {
			//std::cout<<"WORK MANAGER "<<rank<<" PROCESSING COMPLETED TASK FROM WORKER "<<i<<std::endl;
			processCompletedTask(i,tasks[i]);
			//std::cout<<"WORK MANAGER "<<rank<<" PROCESSED COMPLETED TASK FROM WORKER "<<i<<std::endl;
		}
	}
	//std::cout<<"NODE MANAGER: DONE PROCESSING: "<<t.stop()<<std::endl;
};

void processCompletedTask(int workerID, TaskType &t){
	//uint64_t taskID=t.id;

	//extract the systems
	extractSystems(t);

	//process the results
	switch (t.type) {
	case PARSPLICE_TASK_SEGMENT: {
		Trajectory traj=t.trajectory;
		//traj.print();
		statistics.update(traj.front().label,traj.back().label);
		SegmentDatabase db;
		db.add(t.flavor,traj);

		validator.validate(workerID, db);
		break;
	}

	case PARSPLICE_TASK_SYNC_SL: {
		unsigned itrajectory = t.itrajectory; //boost::lexical_cast<unsigned>(t.attributes["itrajectory"]);
		uint64_t label = boost::lexical_cast<uint64_t>(t.attributes["label"]);
		uint64_t batchId = t.batchId; //boost::lexical_cast<uint64_t>(t.attributes["batchId"]);
		std::cout<<"NODEMAN syncstatistics - itrajectory = "<<itrajectory
		         <<" - label = "<<label
		         <<" - batchId = "<<batchId<<std::endl;
		syncstatistics.update(label, itrajectory, batchId);
		break;
	}

	default: {
		//We don't support any other task yet
		assert(t.type==PARSPLICE_TASK_SEGMENT or t.type==PARSPLICE_TASK_SYNC_SL);
		break;
	}

	}
};

void updateStores(){
	Timer t;
	int imax=1000;
	int i=0;
	while(minimaStore->singleServe()>0 and i<imax) {
		i++;
	}
	//std::cout<<"NODE MANAGER: DONE UPDATING STORES: "<<t.stop()<<std::endl;
}

private:
int rank;
int parent;


//Communication variables
MPI_Comm comm;
int recvCompleted;
int sendCompleted;
MPI_Status recvStatus;
MPI_Status sendStatus;
MPI_Request incomingTaskRequest;
MPI_Request outgoingRequest;

unsigned int validatorTimeout;

int nWorkers;
bool everybodyIsDead;
//int nRanksDriver;


std::vector<TaskType> tasks;
std::vector<DriverHandleType> workers;

//task bundle that contains task requests that are in prefetch state
TaskDescriptorBundle prefetches;

//queue of segments that are in ready state
SegmentQueue segmentQueue;

//queue of synchronization tasks that are ready
SegmentQueue syncQueue;

//active reservations to the minimaStore
std::map< std::pair<unsigned int, uint64_t>, uint64_t> activeReservations;

Validator validator;

AbstractDDS *minimaStore;
AbstractLocalDataStore *qsdStore;


TransitionStatistics statistics;
SyncStatistics syncstatistics;

std::map< std::pair<int,int>, std::map<std::string,std::string> > parameters;

std::vector<char> rbuf;
std::vector<char> sbuf;
int rbufSize;
bool die;
};





#endif /* defined(__ParSplice__NodeManager__) */
