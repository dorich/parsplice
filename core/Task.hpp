/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#ifndef Task_hpp
#define Task_hpp

#include <map>
#include <vector>
#include <unordered_set>
#include <random>
#include <limits>
#include <stdio.h>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/map.hpp>
#include <boost/random.hpp>
#include <boost/random/discrete_distribution.hpp>



#include "Types.hpp"
#include "AbstractSystem.hpp"
#include "Pack.hpp"

#define PARSPLICE_TASK_NOTHING 0
#define PARSPLICE_TASK_MD 1
#define PARSPLICE_TASK_MIN 2
#define PARSPLICE_TASK_SEGMENT 3
#define PARSPLICE_TASK_FORCES 4
#define PARSPLICE_TASK_DIE 5
#define PARSPLICE_TASK_INIT_FROM_FILE 6
#define PARSPLICE_TASK_WRITE_TO_FILE 7
#define PARSPLICE_TASK_LABEL 8
#define PARSPLICE_TASK_REMAP 9
#define PARSPLICE_TASK_INIT_VELOCITIES 10
#define PARSPLICE_TASK_CHECK_SL 11
#define PARSPLICE_TASK_SYNC_SL 12
#define PARSPLICE_TASK_MOVE_FIRST 13

const std::map<std::string,int> taskTypeByLabel={
	{ "PARSPLICE_TASK_NOTHING", 0 },
	{ "PARSPLICE_TASK_MD", 1 },
	{ "PARSPLICE_TASK_MIN", 2 },
	{ "PARSPLICE_TASK_SEGMENT", 3 },
	{ "PARSPLICE_TASK_FORCES", 4 },
	{ "PARSPLICE_TASK_DIE", 5 },
	{ "PARSPLICE_TASK_INIT_FROM_FILE", 6 },
	{ "PARSPLICE_TASK_WRITE_TO_FILE", 7 },
	{ "PARSPLICE_TASK_LABEL", 8 },
	{ "PARSPLICE_TASK_REMAP", 9 },
	{ "PARSPLICE_TASK_INIT_VELOCITIES", 10 },
	{ "PARSPLICE_TASK_CHECK_SL", 11 },
	{ "PARSPLICE_TASK_SYNC_SL", 12 },
	{ "PARSPLICE_TASK_MOVE_FIRST", 13 }
};


/**
 * Task class. Describes the tasks that Drivers can execute.
 */

class AbstractTask {
public:
/**
 * Identifies the task type to be carried out
 */
int type;

/**
 * Identifies the task flavor
 */
int flavor;

/**
 * Identifies the task to be carried out
 */
uint64_t id;
uint64_t batchId;
unsigned itrajectory;
std::list< std::vector<int> > changelist; // List of changed relation vectors

};

/**
 * Descriptor of a task to be run.
 */

class TaskDescriptor :  public AbstractTask {
public:

/**
 *
 */
int nInstances;

/**
 * Store the labels of the systems that need to be populated
 */
std::vector<SystemPlaceholder> systems;


/**
 * Assess if dependencies on required Systems are met
 */
bool isRunnable(std::unordered_set<SystemPlaceholder> &available){

	//loop over dependencies
	for(auto it=systems.begin(); it!=systems.end(); it++) {
		//this system must be available
		if(it->necessity==NECESSITY::REQUIRED) {
			SystemPlaceholder s;
			s.type=it->type;
			//by convention, all systems are marked as OPTIONAL in 'available'.
			s.necessity=NECESSITY::OPTIONAL;
			s.label=it->label;

			if(available.count(s)==0) {
				return false;
			}
		}
	}
	return true;
};


void dependencies(std::list<SystemPlaceholder> &deps){

	//loop over dependencies
	for(auto it=systems.begin(); it!=systems.end(); it++) {
		//this system must be available
		if(it->necessity==NECESSITY::REQUIRED) {
			deps.push_back(*it);
		}
	}
};



template<class Archive>
void serialize(Archive & ar, const unsigned int version){
	ar & type;
	ar & id;
	ar & flavor;
	ar & nInstances;
	ar & batchId;
	ar & systems;
	ar & itrajectory;
	ar & changelist;
}
};

class SegmentQueue {
public:

SegmentQueue(){
	boost::random::random_device rd;
	rng.seed(rd());
};

void insert( TaskDescriptor &t){
	if(t.type == PARSPLICE_TASK_SYNC_SL) {
		// only need to do a SYNC task once
		bool inqueue = false;
		for (auto it=tasks.begin(); it!=tasks.end(); it++ ) {
			if(it->second.systems[0].label == t.systems[0].label \
			   and it->second.systems[1].label == t.systems[1].label) inqueue = true;
			if(it->second.systems[1].label == t.systems[0].label \
			   and it->second.systems[0].label == t.systems[1].label) inqueue = true;
		}
		if (!inqueue) tasks.push_back(std::make_pair(uint64_t(1),t));
	}else{
		tasks.push_back(std::make_pair(uint64_t(t.nInstances),t));
	}
};

int size(){
	return tasks.size() + reserve.size();
};

bool next(TaskDescriptor &t){
	// only used for syncQueue
	//if(tasks.size()==0 and reserve.size()==0) return false;
	if(tasks.size()>0) {
		// pick first in list
		auto it=tasks.begin();
		t=it->second;
		std::cout<<"BEFORE: "<<tasks.size();
		tasks.erase(it);
		std::cout<<" AFTER: "<<tasks.size()<<std::endl;
		return true;
	}
	return false;
}

bool sample(TaskDescriptor &t){
	if(tasks.size()==0 and reserve.size()==0) {
		return false;
	}

	if(tasks.size()>0) {


		//pick a random element
		boost::random::uniform_int_distribution<> d(0,tasks.size()-1);

		/*
		   std::vector<int> w;
		   for(auto it=tasks.begin(); it!=tasks.end(); it++) {
		        w.push_back(it->second.nInstances);
		   }


		   boost::random::discrete_distribution<> d(w.begin(),w.end());
		 */

		auto it=tasks.begin();
		std::advance(it, d(rng));

		//extract the next task descriptor
		t=it->second;
		t.nInstances=1;
		//decrement the number of tasks that are still to be executed
		it->first-=1;


		//move to the back or to the reserve
		auto tt=*it;
		tasks.erase(it);

		//move to the reserve
		if(tt.first==0) {
			tt.first=tt.second.nInstances;
			reserve.push_back(tt);
		}
		//move to the back
		else{
			tasks.push_back(tt);
		}
	}
	//pick from the reserve
	else{
		t=reserve.front().second;
		t.nInstances=1;

		//decrement the number of tasks that are still to be executed
		reserve.front().first-=1;

		if(reserve.front().first==0) {
			auto tt=reserve.front();
			tt.first=tt.second.nInstances;
			reserve.pop_front();
			reserve.push_back(tt);
		}
	}

	return true;
};

void purge(){
	//std::cout<<"PURGE"<<std::endl;
	uint64_t maxbatchId=0;
	for(auto it=tasks.begin(); it!=tasks.end(); it++) {
		maxbatchId=std::max(maxbatchId,it->second.batchId);
	}
	for(auto it=reserve.begin(); it!=reserve.end(); it++) {
		maxbatchId=std::max(maxbatchId,it->second.batchId);
	}
	//std::cout<<"MAX BATCH ID: "<<maxbatchId<<std::endl;

	for(auto it=tasks.begin(); it!=tasks.end(); ) {
		if( it->second.batchId!=maxbatchId ) {
			it=tasks.erase(it);
		}
		else{
			it++;
		}
	}
	for(auto it=reserve.begin(); it!=reserve.end(); ) {
		if( it->second.batchId!=maxbatchId ) {
			it=reserve.erase(it);
		}
		else{
			it++;
		}
	}

};



void requiredSystems( std::unordered_set<SystemPlaceholder> &required){
	for(auto it=tasks.begin(); it!=tasks.end(); it++) {
		for(auto it2=it->second.systems.begin(); it2!=it->second.systems.end(); it2++) {
			if(it2->necessity==NECESSITY::REQUIRED) {
				required.insert( *it2 );
			}
		}
	}
	for(auto it=reserve.begin(); it!=reserve.end(); it++) {
		for(auto it2=it->second.systems.begin(); it2!=it->second.systems.end(); it2++) {
			if(it2->necessity==NECESSITY::REQUIRED) {
				required.insert( *it2 );
			}
		}
	}
};

private:
std::list< std::pair<uint64_t, TaskDescriptor> > tasks;
std::list< std::pair<uint64_t, TaskDescriptor> > reserve;
boost::random::mt11213b rng;
};












/**
 * A bundle of task descriptors.
 */
class TaskDescriptorBundle {

public:


void insert(TaskDescriptorBundle &t){
	tasks.splice(tasks.end(),t.tasks);
	t.clear();
};

void insert(TaskDescriptor &t){
	tasks.push_back(t);
};

bool extractRunnable(std::unordered_set<SystemPlaceholder> &available, TaskDescriptor &t){
	for(auto it=tasks.begin(); it!=tasks.end(); it++) {
		if(it->isRunnable(available)) {
			t=*it;
			tasks.erase(it);
			return true;
		}
	}
	return false;
};

void clear(){
	tasks.clear();
};

template<class Archive>
void serialize(Archive & ar, const unsigned int version){
	ar & tasks;
};


void requiredSystems( std::unordered_set<SystemPlaceholder> &required){
	for(auto it=tasks.begin(); it!=tasks.end(); it++) {
		for(auto it2=it->systems.begin(); it2!=it->systems.end(); it2++) {
			if(it2->necessity==NECESSITY::REQUIRED) {
				required.insert( *it2 );
			}
		}
	}
};


std::list<TaskDescriptor>::iterator begin(){
	return tasks.begin();
};

std::list<TaskDescriptor>::iterator end(){
	return tasks.end();
};

//private:

std::list<TaskDescriptor> tasks;
};

template < class System > class Task :  public AbstractTask {
public:

Task(){
	type=PARSPLICE_TASK_NOTHING;
};

Task(TaskDescriptor &t){
	type=t.type;
	flavor=t.flavor;
	id=t.id;
};

/**
 * Task parameters passed from ParSplice (e.g., timestep, number of steps, etc.)
 */
std::map<std::string, std::string > parameters;
/**
 * Attributes of the task. Can be used by the MD engine to cache info.
 */
std::map<std::string, std::string > attributes;
/**
 * Vector of systems over which to operate
 */
std::vector<System> systems;

Trajectory trajectory;

template<class Archive>
void serialize(Archive & ar, const unsigned int version){
	ar & type;
	ar & id;
	ar & flavor;
	ar & parameters;
	ar & attributes;
	ar & trajectory;
	ar & systems;
	ar & itrajectory;
	ar & batchId;
	ar & changelist;
}

void clear(){
	id=0;
	type=-1;
	parameters.clear();
	attributes.clear();
	systems.clear();
	batchId=0;
	changelist.clear();
};


private:

};



#endif /* Task_hpp */
