/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#ifndef Types_h
#define Types_h



#include <stdio.h>
#include <vector>
#include <list>
#include <deque>
#include <memory>
#include <set>
#include <atomic>
#include <future>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/unordered_collections_save_imp.hpp>
#include <boost/serialization/unordered_collections_load_imp.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <boost/serialization/deque.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/deque.hpp>
#include <boost/functional/hash.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random.hpp>
#include <chrono>

typedef uint64_t Label;
typedef char Rdt;
typedef std::vector<Rdt> Rd;




class Timer {
public:
Timer(){
	t0 = std::chrono::high_resolution_clock::now();
}
int stop(){
	auto t1 = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count();
}
private:
std::chrono::time_point<std::chrono::high_resolution_clock> t0;
};

class AbstractSpinLock {

public:
virtual void lock()=0;
virtual void unlock()=0;
};

class SpinLock : public AbstractSpinLock {
std::atomic_flag locked = ATOMIC_FLAG_INIT;
public:
void lock() {
	while (locked.test_and_set(std::memory_order_acquire)) {; }
}
void unlock() {
	locked.clear(std::memory_order_release);
}
};

class NoSpinLock : public AbstractSpinLock {

public:
void lock(){
};
void unlock(){
};
};

struct SyncData {
	Label label;
	uint64_t batchId;
	SyncData(){
		label=0;
		batchId=0;
	};
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & label;
		ar & batchId;
	};
};

struct SyncStatistics {
	SyncStatistics();

	void update(Label finalLabel, unsigned itrajectory, uint64_t batchId);

	void clear();

	int size();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & syncMap;
	};
  std::map<unsigned, SyncData> syncMap;
};

struct StateStatistics {
	StateStatistics();
	StateStatistics(uint64_t label_);

	void update(uint64_t finalState);

	void update(StateStatistics s);

	std::pair<Label,unsigned> sampleBKL(unsigned defaultDuration, double &w, boost::random::mt11213b &rand, boost::random::uniform_01<> &uniform);

	Label sampleSegmentBKL(boost::random::mt11213b &rand, boost::random::uniform_01<> &uniform);

	void clear();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & label;
		ar & nSegments;
		ar & nTransitions;
		ar & counts;
	};

	uint64_t label;
private:
	int nSegments;
	int nTransitions;
	std::unordered_map< uint64_t, int > counts;

};

struct TransitionStatistics {

	TransitionStatistics();

	void clear();

	void update(uint64_t initialState, uint64_t finalState);

	void assimilate(TransitionStatistics &s);

	std::pair<Label,unsigned> sampleBKL(Label &lb, unsigned defaultDuration, double &w);

	int size();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & statistics;
	};


	std::unordered_map< uint64_t, StateStatistics > statistics;
private:
	boost::random::mt11213b rng;
	boost::random::uniform_01<> uniform;

};

struct BoxData {
  std::vector<int> loc;
  std::array<double, 3> xlo;
  std::array<double, 3> xhi;
  std::array<double, 3> dshift;
  BoxData(std::vector<int> loc_in){
    xlo[0] = 0.0; xlo[1] = 0.0; xlo[2] = 0.0;
    xhi[0] = 0.0; xhi[1] = 0.0; xhi[2] = 0.0;
    dshift[0] = 0.0; dshift[1] = 0.0; dshift[2] = 0.0;
    loc = loc_in;
  };
};

struct SyncStatus {

  std::list<std::vector<int>> changelist;         // Ordered list of items in neighlabels
  std::map<std::vector<int>, Label> neighlabels;  // Map of neighbor labels (val) and relation vectors (kys)
  std::map<std::vector<int>, Label> neighlabelsI; //   Like neighlabels, but BEFORE transition
  unsigned int length;                            // Number of changed neighbors
	uint64_t batchId;                               // Unique ID of the batch this SYNC was meant for
  std::map<std::vector<int>, unsigned> SLneighs;  // SLD neighbors (relation vector, jtrajectory)
                                                  // relation vector -> (-1,-1,-1) through (1,1,1)

  SyncStatus(unsigned itrajectory, std::vector<int> NxyzSL, std::vector<int> xyzSL){
    batchId = 0;
    // itrajectory -> id of 'this' trajectory
    // NxyzSL      -> number of SLD's in each direction
    // xyzSL       -> location of "this" SLD in each direction
    int kmin = -1; int kmax = 1;
    if(NxyzSL[2]<2){kmin = 0; kmax = 0;}
    int jmin = -1; int jmax = 1;
    if(NxyzSL[1]<2){jmin = 0; jmax = 0;}
    int imin = -1; int imax = 1;
    if(NxyzSL[0]<2){imin = 0; imax = 0;}
    for(int k=kmin; k<kmax+1; k++){
      int kinc = NxyzSL[0]*NxyzSL[1];
      int kuse = k;
      if((xyzSL[2]+k)<0)             kuse =  (NxyzSL[2]-1);
      if((xyzSL[2]+k)>(NxyzSL[2]-1)) kuse = -(xyzSL[2]);
      for(int j=jmin; j<jmax+1; j++){
        int jinc = NxyzSL[0];
        int juse = j;
        if((xyzSL[1]+j)<0)             juse =  (NxyzSL[1]-1);
        if((xyzSL[1]+j)>(NxyzSL[1]-1)) juse = -(xyzSL[1]);
        for(int i=imin; i<imax+1; i++){
          if(i==0 and j==0 and k==0) continue;
          int iinc = 1;
          int iuse = i;
          if((xyzSL[0]+i)<0)             iuse =  (NxyzSL[0]-1);
          if((xyzSL[0]+i)>(NxyzSL[0]-1)) iuse = -(xyzSL[0]);
          int jtrajectory = itrajectory + kuse*kinc + juse*jinc + iuse*iinc;
          SLneighs[{i,j,k}] = (unsigned) jtrajectory;
          //std::cout<<"ITRAJECTORY "<<itrajectory<<" ADDING JTRAJECTORY "<<jtrajectory
          //         <<" at "<<i<<","<<j<<","<<k<<" with "<<iuse<<","<<juse<<","<<kuse<<std::endl;
        }
      }
    }
    // Initialize 'Changes'
    length = 0;
    batchId = 0;
  };

  void clear(){
    changelist.clear();
    neighlabels.clear();
    neighlabelsI.clear();
    length = 0;
    batchId = 0;
  };

  void print(unsigned itrajectory){
    std::cout<<"=-=-=-=-=-=-=-=-="<<std::endl;
    std::cout<<"neighlabelsI for itrajectory "<<itrajectory<<":"<<std::endl;
  	for(auto it=neighlabelsI.begin(); it!=neighlabelsI.end(); it++) {
  		std::cout<<it->first[0]<<","<<it->first[1]<<","<<it->first[2]
               <<" -> "<<it->second<<std::endl;
  	}
    std::cout<<"neighlabels for itrajectory "<<itrajectory<<":"<<std::endl;
  	for(auto it=neighlabels.begin(); it!=neighlabels.end(); it++) {
  		std::cout<<it->first[0]<<","<<it->first[1]<<","<<it->first[2]
               <<" -> "<<it->second<<std::endl;
  	}
  };

  void addnew(unsigned jtrajectory, Label oldlabel, Label newlabel){
    // Check if jtrajectory is a neighbor, and add new label if it is
    for(auto it=SLneighs.begin(); it!=SLneighs.end(); ++it){
      if(it->second==jtrajectory){
        //std::cout<<"YES jtrajectory "<<jtrajectory<<" is a neghbor."<<std::endl;
        neighlabelsI[it->first] = oldlabel;
        neighlabels[it->first]  = newlabel;
        changelist.push_back(it->first);
        length = neighlabels.size();
      }
    }
  };

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
    ar & changelist;
		ar & neighlabelsI;
		ar & neighlabels;
    ar & length;
		ar & batchId;
		ar & SLneighs;
	};

};

struct Visit {
	Label label;
	unsigned int duration;

	Visit(){
		duration=0;
	};

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & label;
		ar & duration;
	};

};


struct Trajectory {
	Trajectory();

	void clear();

	//append a visit to a trajectory. Extends the last visit if possible
	void appendVisit(Visit &v, bool extend=true);

	//splice two trajectories
	bool splice(Trajectory &t);

	bool empty();

	//uint64_t size();

	uint64_t duration();

	void print();

	Visit& back();

	Visit& front();

	void pop_back();

	void pop_front();

	void reduce_length(int n);

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & visits;
		ar & length;
		ar & index;
		ar & flavor;
	};

	int flavor;

	int color;                  // SL Algorithm (0-7)
	int blocksThisCycle;        // Blocks splice durring the current cycle

	// Current KMC prediction map for this trajectory:
	std::unordered_map<Label, std::multiset<unsigned> > predm;
	//std::unordered_map<Label, unsigned > localpredict;
	int nKMCPred;

	std::list<Visit> visits;

private:
	uint64_t length;
	unsigned int index;

};


class SegmentDatabase {

public:

void clear();

void add(int flavor, Trajectory &t);

void merge(SegmentDatabase &supp);

int size();

int count(Label lb, int flavor);

bool front(Label lb, int flavor, Trajectory &t);

void pop_front(Label lb, int flavor);

void front_split(Label lb, int flavor, Trajectory &t, int n);

void print();

template<class Archive>
void serialize(Archive & ar, const unsigned int version){
	ar & db;
};

std::unordered_map< std::pair<Label, int>, std::deque<Trajectory>, boost::hash <std::pair <Label, int> > > db;

};

struct TADParms {
	double delta;
  double numin;
  double Tlow;
  double Thigh;
  double Tinit;
};


#endif /* Types_h */
