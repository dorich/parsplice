/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#ifndef AbstractEngine_h
#define AbstractEngine_h

#include "Task.hpp"
#include "Graph.hpp"
#include "Types.hpp"

#include <mpi.h>

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random.hpp>
#include <boost/timer/timer.hpp>


class ParameterParser {
public:

ParameterParser() {
	boost::random::random_device rd;
	rng.seed(rd());
	count=1;
}

void seed(int s){
	rng.seed(s);
};

std::string parse(std::string r, std::map<std::string, std::string> &parameters){
	std::string raw=r;
	//replacements based on parameters
	for(auto it=parameters.begin(); it!=parameters.end(); it++ ) {
		std::string key=it->first;
		key="%"+key+"%";
		boost::trim(key);
		boost::replace_all(raw, key, it->second);
	}
	//standard replacements
	boost::random::uniform_01<> uniform;
	boost::random::uniform_int_distribution<> d(1,1000000);

	std::string key="%RANDU%";
	while(not boost::find_first(raw, key).empty()) {
		int r=d(rng);
		std::string s=boost::str(boost::format("%1%" ) % r );
		boost::replace_first(raw, key, s);
		//std::cout<<"RANDU: "<<r<<std::endl;
	}
	key="%COUNT%";
	while(not boost::find_first(raw, key).empty()) {
		std::string s=boost::str(boost::format("%1%" ) % count );
		boost::replace_first(raw, key, s);
		count++;
	}
	return raw;
};

std::vector<std::string> splitLines(std::string r){
	std::vector< std::string > sc;
	boost::split( sc, r, boost::is_any_of("\n"), boost::token_compress_off );
	return sc;
};

private:
boost::random::mt11213b rng;
int count;

};



/**
 * Abstract base class that defines an interface to a simulation engine.
 * Each engine needs to provide an Engine class derived from this one.
 */
template <class System, class Labeler> class AbstractEngine {

public:

AbstractEngine(boost::property_tree::ptree &config, MPI_Comm localComm_, int seed_) : labeler(config){
	localComm=localComm_;
	seed=seed_;
};

Task<System> process(Task<System> &task){
	//boost::timer::auto_cpu_timer t;

	//std::cout<<"PROCESSING TASK "<<task.type<<std::endl;
	switch(task.type) {
	case PARSPLICE_TASK_MD:
		md_impl(task);
		break;
	case PARSPLICE_TASK_MIN:
		min_impl(task);
		break;
	case PARSPLICE_TASK_SEGMENT:
		segment_impl(task);
		break;
	case PARSPLICE_TASK_FORCES:
		forces_impl(task);
		break;
	case PARSPLICE_TASK_INIT_FROM_FILE:
		file_init_impl(task);
		break;
	case PARSPLICE_TASK_WRITE_TO_FILE:
		file_write_impl(task);
		break;
	case PARSPLICE_TASK_LABEL:
		label_impl(task);
		break;
	case PARSPLICE_TASK_REMAP:
		remap_impl(task);
		break;
	case PARSPLICE_TASK_INIT_VELOCITIES:
		init_velocities_impl(task);
		break;
	case PARSPLICE_TASK_CHECK_SL:
		check_sl_impl(task);
		break;
	case PARSPLICE_TASK_SYNC_SL:
		sync_sl_impl(task);
		break;
	}
	return task;
};

private:

/**
 * Generic implementations in terms of more basic tasks. Whenever possible, these should be
 * overridden in derived classes to provide high performance native implementations.
 */
MPI_Comm localComm;
Labeler labeler;
int seed;

virtual void md_impl(Task<System> &task)=0;
virtual void min_impl(Task<System> &task)=0;
virtual void init_velocities_impl(Task<System> &task)=0;
virtual void forces_impl(Task<System> &task){
};
virtual void file_init_impl(Task<System> &task)=0;
virtual void file_write_impl(Task<System> &task){
};
virtual void check_sl_impl(Task<System> &task)=0;
virtual void sync_sl_impl(Task<System> &task)=0;

/**
 * Implement segment generation in terms of other more basic tasks. Can be overridden in derived classes if the engine can generate segments internally.
 */

/**
 * To do: maybe send heartbeats to manager periodically
 */
virtual void segment_impl(Task<System> &task){
	std::cout<<"GENERATING SEGMENT "<<std::endl;

	//read parameters from the task
	double preCorrelationTime=boost::lexical_cast<double>(task.parameters["PreCorrelationTime"]);
	double postCorrelationTime=boost::lexical_cast<double>(task.parameters["PostCorrelationTime"]);
	double minimumSegmentLength=boost::lexical_cast<double>(task.parameters["MinimumSegmentLength"]);
	double blockTime=boost::lexical_cast<double>(task.parameters["BlockTime"]);
	int nDephasingTrialsMax=boost::lexical_cast<int>(task.parameters["MaximumDephasingTrials"]);
	bool reportIntermediates=boost::lexical_cast<bool>(task.parameters["ReportIntermediates"]);


	int maximumSegmentLength;
	if(task.parameters.count("MaximumSegmentLength")>0 ) {
		maximumSegmentLength=boost::lexical_cast<int>(task.parameters["MaximumSegmentLength"]);
	}
	else{
		maximumSegmentLength=25*minimumSegmentLength;
	}

	//create generic tasks for MD and Min
	Task<System> md;
	md.type=PARSPLICE_TASK_MD;
	md.flavor=task.flavor;
	md.parameters=task.parameters;

	Task<System> min;
	min.type=PARSPLICE_TASK_MIN;
	min.flavor=task.flavor;
	min.parameters=task.parameters;

	Task<System> checksl;
	checksl.type=PARSPLICE_TASK_CHECK_SL;
	checksl.flavor=task.flavor;
	checksl.parameters=task.parameters;

	Task<System> label;
	label.type=PARSPLICE_TASK_LABEL;
	label.flavor=task.flavor;
	label.parameters=task.parameters;

	Task<System> remap;
	remap.type=PARSPLICE_TASK_REMAP;
	remap.flavor=task.flavor;
	remap.parameters=task.parameters;

	Task<System> initVelocities;
	initVelocities.type=PARSPLICE_TASK_INIT_VELOCITIES;
	initVelocities.flavor=task.flavor;
	initVelocities.parameters=task.parameters;

	//std::cout<<preCorrelationTime<<" "<<postCorrelationTime<<" "<<minimumSegmentLength<<" "<<blockTime<<" "<<nDephasingTrialsMax<<" "<<std::endl;


	label.systems.push_back(task.systems[0]);
	process(label);
	//cold point is in systems[0]
	uint64_t initialLabel=label.systems[0].label;
	uint64_t currentLabel=label.systems[0].label;
	label.systems.clear();


	std::cout<<" INITIAL LABEL: "<<initialLabel<<std::endl;


	//if we were provided a hot point, it should be located in systems[1]
	bool dephased=(task.systems.size()>1)and (task.systems[1].type==TYPE::QSD);
	//std::cout<<"DEPHASED "<<dephased<<std::endl;

	//save the initial state
	System initial=(dephased ? task.systems[1] : task.systems[0]);

	//sample thermal velocities
	if(not dephased) {
		initVelocities.systems.push_back(initial);
		process(initVelocities);
		initial=initVelocities.systems[0];
	}

	System current=initial;

	md.systems.push_back(initial);
	min.systems.push_back(initial);
	label.systems.push_back(initial);
	checksl.systems.push_back(task.systems[0]);
	checksl.systems.push_back(task.systems[0]);
	System refhot = task.systems[0];
	System refmin = task.systems[0];

	//we can clean up our systems.
	task.systems.clear();

	int nDephasingTrials=0;

	// Minimize before setting refmin if the dephasing is skipped
	bool checksl_update = not dephased;

	while(not dephased) {
		//dephasing loop
		std::cout<<"DEPHASING "<<std::endl;


		//initialize MD task
		md.systems[0]=initial;

		double elapsedTime=0;
		while( elapsedTime  < preCorrelationTime*0.999999999    ) {
			//std::cout<<elapsedTime<<" "<<preCorrelationTime<<" "<<(elapsedTime  < preCorrelationTime)<<std::endl;
			//run md
			process(md);
			elapsedTime+=blockTime;

			//initialize min task
			min.systems[0]=md.systems[0];

			//minimize
			process(min);

			//label
			label.systems[0]=min.systems[0];
			process(label);
			currentLabel=label.systems[0].label;

			std::cout<<"CURRENT LABEL: "<<currentLabel<<std::endl;

			if( currentLabel == initialLabel ) {
				dephased=true;
			}
			else{
				dephased=false;
				break;
			}
		}

		nDephasingTrials++;
		if(nDephasingTrials>=nDephasingTrialsMax) {
			break;
		}
	}

	std::cout<<"DEPHASING DONE"<<std::endl<<std::flush;

	// Update reference states for sublatice check
	if(checksl_update) {
		refmin = min.systems[0];
		refhot = md.systems[0];
	}

	bool segmentIsSpliceable=false;
	double lastTransitionTime=-postCorrelationTime;
	int nBlocks=0;
	double elapsedTime=0;

	task.trajectory.clear();
	Visit v;
	v.label=initialLabel;
	v.duration=0;
	task.trajectory.appendVisit(v);

	while( !segmentIsSpliceable or elapsedTime  < minimumSegmentLength*0.999999999 ) {

		//take a block of MD
		//std::cout<<"MD"<<std::endl;
		process(md);
		nBlocks++;
		elapsedTime+=blockTime;

		//append to the trajectory
		if(reportIntermediates) {
			v.label=currentLabel;
		}
		v.duration=1;
		task.trajectory.appendVisit(v);

		//setup the minimization
		min.systems[0]=md.systems[0];
		//std::cout<<"MIN"<<std::endl;
		process(min);

		//hash current state
		uint64_t previousLabel=currentLabel;
		label.systems[0]=min.systems[0];
		//std::cout<<"LABEL"<<std::endl;
		process(label);
		currentLabel=label.systems[0].label;

		//std::cout<<"CURRENT LABEL: "<<currentLabel<<std::endl<<std::flush;

		if( currentLabel!=previousLabel ) {
			if(1) {
				//std::cout<<"run checksl... "<<std::endl;
				// USE SLD - TO CHECK IF A TRANSITION SHOULD BE REJECTED
				checksl.systems[1] = min.systems[0];
				checksl.systems[0] = refmin;
				process(checksl);
				label.systems[0] = checksl.systems[1];
				//std::cout<<"...checksl done "<<std::endl;
			}else if(0) {
				// REJECT EVERYTHIG (JUST FOR TESTING)
				min.systems[0] = refhot;
				process(min);
				label.systems[0] = min.systems[0];
			}
			process(label);
			currentLabel=label.systems[0].label;
			if(currentLabel==previousLabel) {
				std::cout<<"SLD CORRECTION -> CURRENT LABEL: "<<currentLabel<<std::endl;
				md.systems[0] = refhot;
				min.systems[0] = refmin;
				label.systems[0] = refmin;
			}else{
				std::cout<<"TRANSITION DETECTED, "<<previousLabel
				         <<" to "<<currentLabel<<std::endl;

				lastTransitionTime=elapsedTime;
				if(reportIntermediates) {
					label.systems[0].type=TYPE::MINIMUM;
					task.systems.push_back(label.systems[0]);
				}
			}
		}
		refmin = min.systems[0];
		refhot = md.systems[0];

		//std::cout<<"SEGMENT DONE - CURRENT LABEL: "<<currentLabel<<std::endl<<std::flush;
		//a segment is spliceable if the last transition occurred at least postCorrelationTime in the past
		segmentIsSpliceable=(elapsedTime-lastTransitionTime >= postCorrelationTime*0.999999999);

		if(task.trajectory.duration()>=maximumSegmentLength) {
			std::cout<<"SEGMENT EXCEEDED MAXIMUM LENGTH. BAILING OUT."<<std::endl;
			break;
		}

	}
	std::cout<<"SEGMENT DONE "<<task.trajectory.visits.size()<<std::endl;

	if(not reportIntermediates) {
		v.label=currentLabel;
		v.duration=0;
		task.trajectory.appendVisit(v);
	}

	//leave a marker that we had to end the segment prematurely
	if(!segmentIsSpliceable) {
		v.label=666;
		v.duration=0;
		task.trajectory.appendVisit(v);
	}


	min.systems[0].type=TYPE::MINIMUM;
	min.systems[0].label=currentLabel;

	md.systems[0].type=TYPE::QSD;
	md.systems[0].label=currentLabel;

	//remap minimum and qsd to canonical representation
	//insert the final minimum and hot point
	remap.systems.clear();
	remap.systems.push_back(min.systems[0]);
	remap.systems.push_back(md.systems[0]);
	process(remap);

	std::cout<<"REMAPPED LABEL: "<<remap.systems[0].label<<std::endl;
	//append a canonical placeholder to the trajectory
	task.systems.push_back(remap.systems[0]);
	task.systems.push_back(remap.systems[1]);

	//placeholder to signal a switch to a canonical representative
	if(currentLabel!=remap.systems[0].label) {
		v.label=0;
		v.duration=0;
		task.trajectory.appendVisit(v);
	}
	v.label=remap.systems[0].label;
	v.duration=0;
	task.trajectory.appendVisit(v);

};


virtual void label_impl(Task<System> &task){

	bool canonical=false;

	//canonical=true;
	//std::cout<<"LABELING "<<canonical<<std::endl;
	//boost::lexical_cast<bool>(task.parameters["CanonicalLabels"]);
	for(int i=0; i<task.systems.size(); i++) {
		task.systems[i].label=labeler.hash(task.systems[i],canonical);
		//std::cout<<task.systems[i].label<<std::endl;
	}
};

//joint remapping of a number of states. This assumes that all systems in the task correspond to the same state.
//the first one is used as a reference
virtual void remap_impl(Task<System> &task){

	bool canonical=boost::lexical_cast<bool>(task.parameters["CanonicalLabels"]);
	//std::cout<<"REMAPPING "<<canonical<<std::endl;
	if(canonical and task.systems.size()>0) {
		std::map<int,int> canonicalMap;
		Label lb;


		//Label l0=labeler.hash(task.systems[0],false);
		//Label l1=labeler.hash(task.systems[0],true);



		//the reference system is the first one
		labeler.canonicalMap(task.systems[0],canonicalMap,lb);

		/*
		   //swap two entries
		   int s1=canonicalMap[1];
		   int s2=canonicalMap[2];
		   canonicalMap[1]=s2;
		   canonicalMap[2]=s1;
		 */


		//remap all systems
		for(int i=0; i<task.systems.size(); i++) {
			task.systems[i].remap(canonicalMap);
			task.systems[i].label=lb;
		}

		//Label l2=labeler.hash(task.systems[0],false);
		//Label l3=labeler.hash(task.systems[0],true);

		//std::cout<<l0<<" "<<l1<<" "<<l2<<" "<<l3<<std::endl;

	}
};

};


#endif /* AbstractEngine_h */
