/*
 Copyright (c) 2016, Los Alamos National Security, LLC
 All rights reserved.
 Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.
 
 Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Splicer.hpp"

ParSpliceSplicer::ParSpliceSplicer(boost::property_tree::ptree &config) : modifier(config){ };

ParSpliceSplicer::ParSpliceSplicer(MPI_Comm splicerComm_, AbstractDDS *minimaStore_, std::set<int> children_, boost::property_tree::ptree &config, MPI_Comm workerComm) : modifier(config){
  
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer() ";
  splicerComm=splicerComm_;
  usedBlocks=unusedBlocks=wastedBlocks=producedBlocks=0;
  carryOverTime=0;
  minimaStore=minimaStore_;
  children=children_;
  validator.init(children,1);
  
  start=std::chrono::high_resolution_clock::now();
  die=false;
  killed=false;
  
  officialTrajectory.nKMCPred = 0;
  
  //check if checkpoint files are present
  bool restartFromCheckPoint= boost::filesystem::exists("./traj.out.chk") && boost::filesystem::exists("./times.out.chk") && boost::filesystem::exists("./Splicer.chk");
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer() restartFromCheckPoint: "<<restartFromCheckPoint;
  
  if(restartFromCheckPoint) {
    boost::filesystem::copy_file("./traj.out.chk","./traj.out",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out.chk","./times.out",boost::filesystem::copy_option::overwrite_if_exists);
    outTraj.open("./traj.out", std::ios::app);
    outTime.open("./times.out", std::ios::app);
  }
  else{
    outTraj.open("./traj.out", std::ios::out);
    outTime.open("./times.out", std::ios::out);
  }
  
  //initialize from config data
  wMin=config.get<double>("ParSplice.MC.MinimumTrajectoryWeigth",1e-12);
  defaultVisitDuration=config.get<unsigned>("ParSplice.MC.DefaultVisitDuration",100000000);
  predictionGranularity=config.get<unsigned>("ParSplice.MC.PredictionGranularity",1);
  batchSize=config.get<unsigned>("ParSplice.Topology.NWorkers",1);
  predictionTime=config.get<unsigned>("ParSplice.Topology.NWorkers",1);
  maxTaskMesgSize=config.get<int>("ParSplice.MaximumTaskMesgSize",1000000);
  validatorTimeout=config.get<unsigned>("ParSplice.Splicer.ValidatorTimeout",1000000);
  
  broadcastDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.BroadcastDelay",1) );
  reportDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.ReportDelay",10000) );
  checkpointDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.CheckpointDelay",100000) );
  flushOnModify= config.get<bool>("ParSplice.Splicer.FlushOnModify",0);
  runTime=std::chrono::minutes( config.get<unsigned>("ParSplice.RunTime",1000000) );
  
  //read task parameters
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.TaskParameters")) {
    std::string stype=v.second.get<std::string>("Type");
    boost::trim(stype);
    int type=taskTypeByLabel.at(stype);
    int flavor=v.second.get<int>("Flavor");
    BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("")) {
      std::string key=vv.first;
      std::string data=vv.second.data();
      boost::trim(key);
      boost::trim(data);
      std::cout<<key<<" "<<data<<std::endl;
      taskParameters[std::make_pair(type,flavor)][key]=data;
    }
  }
  
  driver=new DriverHandleType(workerComm);
  
  //TODO: FIX ME
  defaultFlavor=1;
  STATE_DBKEY=0;
  batchId=1;
  broadcastRequests=std::vector<MPI_Request>(children.size(),MPI_REQUEST_NULL);
  
  if(restartFromCheckPoint) {
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer(): Restoring checkpoing ";
    // create and open an archive for input
    std::ifstream ifs("Splicer.chk");
    boost::archive::text_iarchive ia(ifs);
    // read class state from archive
    ia >> *this;
    // archive and stream closed when destructors are called
  }
  else{
    addsystem_byfile(config);
  }
  
  std::cout<<"INITIAL TRAJECTORY: "<<std::endl;
  officialTrajectory.print();
  
};

ParSpliceSplicer::~ParSpliceSplicer(){
  delete driver;
};

void ParSpliceSplicer::addsystem_byfile(boost::property_tree::ptree &config){
  
  TaskType task;
  
  std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
  boost::trim(initialConfiguration);
  
  task.type=PARSPLICE_TASK_INIT_FROM_FILE;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,defaultFlavor)];
  task.parameters["Filename"]=initialConfiguration;
  driver->assign(task);
  while(not driver->probe(task)) {};
  task.parameters.clear();
  
  task.type=PARSPLICE_TASK_MIN;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  task.systems[0].type=TYPE::MINIMUM;
  
  task.type=PARSPLICE_TASK_LABEL;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_LABEL,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  
  task.type=PARSPLICE_TASK_REMAP;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  
  Rd data;
  pack(data,task.systems[0]);
  minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
  std::cout<<"DB STORE "<<data.size()<<" "<<task.systems[0].label<<std::endl;
  
  Visit v;
  v.label=task.systems[0].label;
  v.duration=0;
  officialTrajectory.appendVisit(v);
  officialTrajectory.flavor=defaultFlavor;
  
};

void ParSpliceSplicer::server(){
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server ";
  
  lastBroadcast=std::chrono::high_resolution_clock::now();
  lastReport=std::chrono::high_resolution_clock::now();
  lastCheckpoint=std::chrono::high_resolution_clock::now();
  
  //TODO: do this right
  rbufSize=100000000;
  rbuf=std::vector<char>(rbufSize,' ');
  
  //post a  non-blocking receive
  MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,splicerComm,&incomingRequest);
  recvCompleted=0;
  
  //std::vector<std::vector<char> > sBuf(children.size());
  officialTrajectory.nKMCPred=0;
  
  while(true) {
    
    //std::cout<<"updateStores(); "<<std::endl<<std::flush;
    //update databases
    updateStores();
    
    //std::cout<<"processRecv(); "<<std::endl<<std::flush;
    //process non-blocking receives (completed segments from WM)
    processRecv();
    
    //std::cout<<"multiSplice(); "<<std::endl<<std::flush;
    //splice active trajectories as much as possible
    splice();
    
    //std::cout<<"KMC(); "<<std::endl<<std::flush;
    //update KMC predictors
    KMC();
    
    //std::cout<<"processSend(); "<<std::endl<<std::flush;
    //process non-blocking sends (tasks to WM)
    processSend();
    
    //std::cout<<"report(); "<<std::endl<<std::flush;
    //write outputs if needed
    report();
    
    //std::cout<<"checkpoint(); "<<std::endl<<std::flush;
    //checkpoint if needed
    checkpoint();
    
    
    if(killed) {
      std::cout<<"Splicer: Die signal has been sent. Shutting down."<<std::endl;
      
      //kill our own worker
      TaskType task;
      task.type=PARSPLICE_TASK_DIE;
      task.flavor=defaultFlavor;
      task.batchId=batchId;
      task.systems.clear();
      driver->assign(task);
      
      //force a checkpoint
      //checkpoint(true);
      break;
    }
    if(std::chrono::high_resolution_clock::now() - start > runTime  ) {
      die=true;
      std::cout<<"Splicer: Time to shut down"<<std::endl;
    }
    
  }
  
};

void ParSpliceSplicer::updateStores(){
  int imax=1000;
  int i=0;
  while(minimaStore->singleServe()>0 and i<imax) {
    i++;
  }
};

void ParSpliceSplicer::processRecv(){
  //did we receive new completed tasks?
  
  if(die) {
    std::cout<<"Splicer: cancelling pending receives"<<std::endl;
    MPI_Cancel(&incomingRequest);
    MPI_Wait(&incomingRequest,&recvStatus);
  }
  else{
    
    if(not recvCompleted) {
      MPI_Test(&incomingRequest,&recvCompleted,&recvStatus);
    }
    if(recvCompleted) {
      Timer t;
      //extract
      int peer=recvStatus.MPI_SOURCE;
      int tag=recvStatus.MPI_TAG;
      int count=0;
      MPI_Get_count( &recvStatus, MPI_BYTE, &count );
      
      //std::cout<<"SPLICER RECEIVED "<<count<<" BYTES FROM "<<peer<<" WITH TAG "<<tag<<" "<<std::endl;
      //std::vector<char> rrbuf(rbuf.begin(),rbuf.begin()+count);
      //std::cout<<"HASH: "<<boost::hash_value(rrbuf)<<" SIZE: :"<<count<<std::endl;
      //std::cout<<int(rrbuf[0])<<" "<<int(rrbuf[rrbuf.size()-1])<<std::endl;
      
      switch (tag) {
        case PARSPLICE_STATS_SEGMENTS_TAG: {
          //std::cout<<"SPLICER RECEIVED SEGMENTS AND STATS"<<std::endl;
          
          //SegmentDatabase incomingSegments;
          std::list<SegmentDatabase> incomingBatches;
          TransitionStatistics statistics;
          unpack(rbuf,statistics,incomingBatches,std::size_t(count));
          
          for(auto itbatch=incomingBatches.begin(); itbatch!=incomingBatches.end(); itbatch++) {
            for(auto itdb=itbatch->db.begin(); itdb!=itbatch->db.end(); itdb++) {
              for(auto ittraj=itdb->second.begin(); ittraj!=itdb->second.end(); ittraj++) {
                producedBlocks+=ittraj->duration();
              }
            }
          }
          
          //std::cout<<"SPLICER RECEIVED SEGMENTS AND STATS "<<incomingBatches.size()<<" "<<statistics.size()<<std::endl;
          //validate the new batches
          for(auto it=incomingBatches.begin(); it!=incomingBatches.end(); it++) {
            //std::cout<<"SPLICER VALIDATED SEGMENTS "<<std::endl;
            //it->print();
            validator.validate(peer,*it);
          }
          //std::cout<<"SPLICER VALIDATED SEGMENTS "<<std::endl;
          
          //transfer to segmentDB
          while(true) {
            validator.refreshHead(validatorTimeout);
            SegmentDatabase validatedSegments=validator.release();
            if(validatedSegments.size()>0) {
              //std::cout<<"SPLICER MERGING SEGMENTS "<<std::endl;
              //validatedSegments.print();
              segmentDB.merge(validatedSegments);
            }
            else{
              break;
            }
          }
          
          //std::cout<<"SPLICER MERGED SEGMENTS "<<std::endl;
          //update Markov model
          markovModel.assimilate(statistics);
          //std::cout<<"SPLICER UPDATED MARKOV MODEL "<<std::endl;
          
          break;
        }
          
        default: {
          //We don't support any other task yet
          assert(tag==PARSPLICE_STATS_SEGMENTS_TAG or tag==PARSPLICE_SYNC_STATS_TAG);
          break;
        }
          
      }
      
      //post a new receive for tasks
      MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,splicerComm,&incomingRequest);
      recvCompleted=0;
      
    }
  }
};

void ParSpliceSplicer::processSend(){
  
  //std::cout<<"SPLICER DONE KMC "<<std::endl;
  //test for completion of the previous task broadcast
  MPI_Testall(children.size(), &(broadcastRequests[0]),&broadcastCompleted, MPI_STATUSES_IGNORE);
  
  if(die and not killed) {
    std::cout<<"Splicer: killing the workers"<<std::endl;
    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);
    
    TaskDescriptorBundle taskBundle;
    TaskDescriptor task;
    task.type=PARSPLICE_TASK_DIE;
    task.flavor=defaultFlavor;
    task.batchId=batchId;
    task.nInstances=1;
    task.systems.clear();
    taskBundle.insert(task);
    
    int iChild=0;
    for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
      pack(sBuf[iChild],taskBundle);
      //TODO make this more robust
      assert(sBuf[iChild].size()<maxTaskMesgSize);
      MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
    }
    killed=true;
    
    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);
  }
  
  //buffer the predictions and broadcast only if delay has passed since the last time
  //also make sure we are done broadcasting the previous batch
  if(std::chrono::high_resolution_clock::now() - lastBroadcast> broadcastDelay and broadcastCompleted) {
    
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server broadcasting ";
    std::cout<<"SPLICER BROADCASTING"<<std::endl;
    std::cout<<"CURRENT HEAD(S): "<<std::endl;
    std::cout<<officialTrajectory.back().label <<std::endl;
    
    //reset the send buffer
    sBuf=std::vector<std::vector<char> >(children.size());
    
    {
      
      //TODO: make this faster
      std::map<Label, unsigned > counts;
      int nKeep=0;
      
      for(auto it=officialTrajectory.predm.begin(); it!=officialTrajectory.predm.end(); it++) {
        it->second.erase(0);
      }
      
      std::set<SchedulerState> schedulingSet;
      for(auto it=officialTrajectory.predm.begin(); it!=officialTrajectory.predm.end(); it++) {
        SchedulerState s;
        s.label=it->first;
        s.counts=it->second;
        if(s.counts.size()>0) {
          schedulingSet.insert(s);
        }
      }
      
      while(nKeep<batchSize and schedulingSet.size()>0) {
        //find the state with the largest number of KMC instances that required more segments
        auto it=schedulingSet.rbegin();
        
        SchedulerState s=*it;
        //remove from the set
        schedulingSet.erase(std::next(it).base());
        for(int i=0; i<predictionGranularity; i++) {
          nKeep++;
          counts[s.label]+=1;
          s.counts.erase(counts[s.label]);
        }
        //reinsert if there are KMC instances left
        if(s.counts.size()>0) {
          //hint that the updated state might still be close to the back
          schedulingSet.insert(schedulingSet.cend(),s);
        }
      }
      
      //clear the previous predictions
      officialTrajectory.predm.clear();
      officialTrajectory.nKMCPred=0;
      
      //split the load between children
      //TODO: This has to be improved to consider affinity
      unsigned nTaskPerChild=(children.size()>0 ? nKeep/children.size() : 0);
      unsigned np=nKeep%children.size();
      
      //pack and send the messages
      int iChild=0;
      for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
        
        //create the task bundle
        TaskDescriptorBundle taskBundle;
        TaskDescriptor task;
        task.type=PARSPLICE_TASK_SEGMENT;
        task.flavor=defaultFlavor;
        task.batchId=batchId;
        unsigned n=0;
        unsigned nt=nTaskPerChild+(iChild<np ? 1 : 0);
        for(auto itc=counts.begin(); itc!=counts.end(); ) {
          unsigned nInstances=itc->second;
          Label lb=itc->first;
          
          //we can take in all of these tasks
          if(n+nInstances<nt) {
            n+=nInstances;
            itc=counts.erase(itc);
          }
          //we can only take part of these tasks
          else{
            nInstances=nt-n;
            n=nt;
            itc->second-=nInstances;
          }
          
          std::cout<<"SCHEDULING "<<nInstances<<" INSTANCES IN STATE " <<lb<<" FOR WORKER "<<*it<<std::endl;
          
          task.nInstances=nInstances;
          task.systems.clear();
          SystemPlaceholder s;
          s.label=lb;
          s.necessity=NECESSITY::REQUIRED;
          s.type=TYPE::MINIMUM;
          task.systems.push_back(s);
          s.necessity=NECESSITY::OPTIONAL;
          s.type=TYPE::QSD;
          task.systems.push_back(s);
          
          taskBundle.insert(task);
          
          if(n==nt) {
            break;
          }
        }
        //sBuf[iChild].clear();
        pack(sBuf[iChild],taskBundle);
        //std::cout<<splicerComm<<" "<<&(sBuf[iChild][0])<<" "<<sBuf[iChild].size()<<" "<<*it<<" "<<PARSPLICE_TASK_TAG <<" "<< &(broadcastRequests[iChild])<<std::endl;
        
        /*
         {
         //test
         TaskDescriptorBundle tb;
         unpack(sBuf[iChild],tb,sBuf[iChild].size());
         std::cout<<"HASH S "<<boost::hash_range(sBuf[iChild].begin(),sBuf[iChild].end())<<" "<<sBuf[iChild].size()<<std::endl;
         }
         */
        
        //TODO make this more robust
        assert(sBuf[iChild].size()<maxTaskMesgSize);
        MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
      }
    }
    lastBroadcast=std::chrono::high_resolution_clock::now();
    
    std::cout<<"SPLICER DONE BROADCASTING "<<t.stop()<<std::endl;
    batchId++;
  }
};


void ParSpliceSplicer::splice(){
  Timer t;
  unsigned long usedBefore;
  
  Label previousLabel=officialTrajectory.back().label;
  
  bool spliced=false;
  while(usedBefore=usedBlocks, spliceOne()) {
    spliced=true;
    if(modifier.modificationNeeded(usedBefore,usedBlocks))
    {
      std::cout<<"MODIFY "<<officialTrajectory.back().label<<std::endl;
      Rd data;
      SystemType s;
      uint64_t res=minimaStore->reserve(STATE_DBKEY,officialTrajectory.back().label);
      while(not minimaStore->get(STATE_DBKEY, officialTrajectory.back().label,data)) {minimaStore->singleServe();}
      minimaStore->release(res);
      unpack(data,s,0);
      
      modifier.modify(s, usedBefore, usedBlocks);
      
      std::cout<<"MODIFY DONE"<<std::endl;
      TaskType task;
      task.systems.push_back(s);
      
      task.type=PARSPLICE_TASK_MIN;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      std::cout<<"MIN DONE"<<std::endl;
      
      task.type=PARSPLICE_TASK_REMAP;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      std::cout<<"REMAP DONE "<<task.systems[0].label<<std::endl;
      
      data.clear();
      pack(data,task.systems[0]);
      minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
      std::cout<<"PUT DONE"<<std::endl;
      
      Visit v;
      v.label=task.systems[0].label;
      v.duration=0;
      officialTrajectory.appendVisit(v);
      std::cout<<"APPEND DONE"<<std::endl;
      
      if(flushOnModify) {
        markovModel.clear();
        segmentDB.clear();
      }
    }
    
  }
  if(officialTrajectory.back().label!=previousLabel) {
    officialTrajectory.predm.clear();
    officialTrajectory.nKMCPred=0;
  }
  if(spliced) {
    //std::cout<<"DONE SPLICING: "<<t.stop()<<std::endl;
  }
  
};

void ParSpliceSplicer::KMC(){
  if(officialTrajectory.nKMCPred<100) {
    //generate a virtual trajectory
    predictions.clear();
    KMCSchedule(predictions);
    //std::cout<<"CURRENT HEAD "<< officialTrajectory.back().label <<std::endl;
    for(auto it=predictions.begin(); it!=predictions.end(); it++) {
      //std::cout<<"PREDICTION: "<<it->first<<" "<<it->second<<std::endl;
      officialTrajectory.predm[it->first].insert(it->second);
    }
    //std::cout<<"DONE KMC: "<<t.stop()<<std::endl;
  }
  officialTrajectory.nKMCPred++;
};

void ParSpliceSplicer::report(){
  //report at given intervals
  if(std::chrono::high_resolution_clock::now() - lastReport> reportDelay  ) {
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting ";
    
    //output timings
    outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;
    
    //output trajectory
    int iVisit=0;
    Visit back=officialTrajectory.back();
    officialTrajectory.pop_back();
    
    std::stringstream ss;
    for(auto it=officialTrajectory.visits.begin(); it!=officialTrajectory.visits.end(); it++) {
      ss<<" "<<it->label<<" "<<it->duration<<" 0\n"; // Not SLParSplice, so always the "0th" trajectory
      iVisit++;
    }
    outTraj<<ss.str();
    outTraj.flush();
    
    officialTrajectory.clear();
    officialTrajectory.appendVisit(back);
    
    lastReport=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting done ";
    std::cout<<"SPLICER DONE REPORTING "<<t.stop()<<std::endl;
  }
};

void ParSpliceSplicer::checkpoint(bool now){
  //checkpoint at given intervals
  if(now or std::chrono::high_resolution_clock::now() - lastCheckpoint> checkpointDelay  ) {
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing ";
    //checkpoint();
    outTraj.close();
    outTime.close();
    
    //copy consistent progress files
    boost::filesystem::copy_file("./traj.out","./traj.out.chk",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out","./times.out.chk",boost::filesystem::copy_option::overwrite_if_exists);
    
    outTraj.open("./traj.out", std::ios::app | std::ios::out);
    outTime.open("./times.out", std::ios::app | std::ios::out);
    
    
    std::ofstream ofs("./Splicer.chk");
    // save data to archive
    {
      boost::archive::text_oarchive oa(ofs);
      // write class instance to archive
      oa << *this;
      // archive and stream closed when destructors are called
    }
    
    std::cout<<"CHECKPOINTING"<<std::endl;
    lastCheckpoint=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing done";
  }
};


void ParSpliceSplicer::KMCSchedule( std::unordered_map<Label, unsigned > & visits){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC ";
  
  
  //keep track of which segment in the database we already used
  std::unordered_map<Label, int> consumedSegments;
  
  //start at the current end
  Label c=officialTrajectory.back().label;
  double w=1;
  unsigned length=0;
  std::pair<Label, unsigned> s;
  {
    //boost::timer::auto_cpu_timer t(3, "KMC scheduling loop: %w seconds\n");
    
    while(true) {
      //"virtually" consume segment from the database
      auto key=std::make_pair(c,defaultFlavor);
      
      if(segmentDB.count(c,defaultFlavor)>0 and consumedSegments[c]< segmentDB.count(c,defaultFlavor) ) {
        Trajectory &t=segmentDB.db[key][consumedSegments[c]];
        consumedSegments[c]+=1;
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC virtual consume "<<c<<" "<<t.back().label;
        //std::cout<<"Splicer KMC virtual consume "<<c<<" "<<t.back().label<<std::endl;
        c=t.back().label;
      }
      //generate segment with BKL
      else{
        s=markovModel.sampleBKL(c, defaultVisitDuration, w);
        //std::cout<<"#Splicer KMC generate "<<c<<" "<<s.first<<" "<<s.second<<" "<<w<<std::endl;
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC generate "<<c<<" "<<s.first<<" "<<s.second<<" "<<w;
        int duration=s.second;
        Visit v;
        v.label=c;
        //do not exceed the prediction horizon
        v.duration=( duration+length>predictionTime ? predictionTime-length : duration );
        length+=v.duration;
        //visits.push_back(v);
        visits[v.label]+=v.duration; // RJZ: Should we be using += if v.label is not in visits yet??
        c=s.first;
        //break if weight becomes too low or we reached the prediction time
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC length "<<length<<" "<<v.duration<<" "<<s.second;
        if(length>=predictionTime or w<wMin) {
          break;
        }
      }
    }
  }
};

std::unordered_map<Label, int> ParSpliceSplicer::KMCScheduleAbs( std::unordered_map<Label, unsigned > & visits){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC ";
  
  //list of segments we need
  std::unordered_map<Label, int> scheduledSegments;
  
  //start at the current end
  Label cStart=officialTrajectory.back().label;
  
  int nScheduled=0;
  
  double w=1;
  unsigned length=0;
  std::pair<Label, unsigned> s;
  while (nScheduled<predictionTime) {
    
    //Start a KMC simulation from cStart
    Label c=cStart;
    length=0;
    //boost::timer::auto_cpu_timer t(3, "KMC scheduling loop: %w seconds\n");
    
    
    //keep track of segments that were already used
    std::unordered_map<Label, int> consumedSegments;
    std::unordered_map<Label, int> consumedScheduledSegments;
    
    while(true) {
      //"virtually" consume segment from the database
      auto key=std::make_pair(c,defaultFlavor);
      
      //there is a corresponding segment in the database, consume it
      if(segmentDB.count(c,defaultFlavor)>0 and consumedSegments[c]< segmentDB.count(c,defaultFlavor) ) {
        Trajectory &t=segmentDB.db[key][consumedSegments[c]];
        consumedSegments[c]+=1;
        c=t.back().label;
      }
      //no corresponding segment in the database
      else{
        //sample end state with BKL
        s=markovModel.sampleBKL(c, defaultVisitDuration, w);
        
        //additional number of segments needed to cover this move
        int ns=s.second-(scheduledSegments.count(c)-consumedScheduledSegments[c]);
        
        
        //we have enough scheduled segments to cover this move, no need to schedule more
        if(ns<=0) {
          consumedScheduledSegments[s.first]+=s.second;
          c=s.first;
        }
        //we don't have enough segments to cover this move: schedule more segments and restart a KMC run
        else{
          scheduledSegments[c]+=ns;
          nScheduled+=ns;
          break;
        }
      }
    }
  }
  return scheduledSegments;
};


bool ParSpliceSplicer::spliceOne(){
  
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer::splice ";
  //add segments to the official trajectory
  Label end=officialTrajectory.back().label;
  //BOOST_LOG_SEV(lg,info) <<"#Splicer Splice Current end: "<<end;
  auto key=std::make_pair(end,defaultFlavor);
  
  if( segmentDB.count(end,defaultFlavor)> 0 ) {
    
    Trajectory t;
    
    // Removed the entire trajectory at 'end'.. we can use everything
    segmentDB.front(end,defaultFlavor,t);
    segmentDB.pop_front(end,defaultFlavor);
    
    usedBlocks+=t.duration();
    //unusedBlocks-=t.size();
    officialTrajectory.splice(t);
    end=officialTrajectory.back().label;
    key=std::make_pair(end,defaultFlavor);
    //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;
    return true;
  }
  else{
    return false;
  }
  
};

#if 0
bool ParSpliceSplicer::splice(){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer::splice ";
  //add segments to the official trajectory
  Label end=officialTrajectory.back().label;
  //BOOST_LOG_SEV(lg,info) <<"#Splicer Splice Current end: "<<end;
  //std::cout<<"#Splicer Splice Current end: "<<end<<" "<<std::endl;
  Label originalEnd=end;
  auto key=std::make_pair(end,defaultFlavor);
  
  while( segmentDB.count(end,defaultFlavor)> 0 ) {
    //std::cout<<"#Splicer count: "<<segmentDB.count(end,defaultFlavor)<<std::endl;
    Trajectory t;
    segmentDB.front(end,defaultFlavor,t);
    segmentDB.pop_front(end,defaultFlavor);
    usedBlocks+=t.duration();
    //unusedBlocks-=t.size();
    officialTrajectory.splice(t);
    end=officialTrajectory.back().label;
    key=std::make_pair(end,defaultFlavor);
    //std::cout<<"#Splicer Splice Current end: "<<end<<" "<<std::endl;
    //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;
  }
  
  //officialTrajectory.print();
  return originalEnd != end;
};
#endif

void ParSpliceSplicer::output(){
  
  //output timings
  outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;
  
  //output trajectory
  int iVisit=0;
  Visit back=officialTrajectory.back();
  officialTrajectory.pop_back();
  
  std::stringstream ss;
  for(auto it=officialTrajectory.visits.begin(); it!=officialTrajectory.visits.end(); it++) {
    ss<<" "<<it->label<<" "<<it->duration<<" 0\n"; // Not SLParSplice, so always the "0th" trajectory
    iVisit++;
  }
  outTraj<<ss.str();
  outTraj.flush();
  
  officialTrajectory.clear();
  officialTrajectory.appendVisit(back);
  
};






// Constructor:
SLParSpliceSplicer::SLParSpliceSplicer(MPI_Comm splicerComm_, AbstractDDS *minimaStore_, std::set<int> children_, boost::property_tree::ptree &config, MPI_Comm workerComm) :
  ParSpliceSplicer::ParSpliceSplicer(config) {
  
  splicerComm=splicerComm_;
  usedBlocks=unusedBlocks=wastedBlocks=producedBlocks=0;
  carryOverTime=0;
  minimaStore=minimaStore_;
  children=children_;
  validator.init(children,1);
  
  start=std::chrono::high_resolution_clock::now();
  die=false;
  killed=false;
  
  //How many independent trajectories to splice:
  ntrajectories=config.get<unsigned>("ParSplice.MultiTrajectories.ntrajectories",1);
  if(ntrajectories<1) ntrajectories=1;
  ncolors=config.get<unsigned>("ParSplice.MultiTrajectories.ncolors",1);
  if((ntrajectories%ncolors)!=0) ncolors=1;
  
  //Check if Sub-lattice Decomposition will be used:
  useSubLattice=config.get<unsigned>("ParSplice.MultiTrajectories.useSubLattice",0);
  if(useSubLattice) {
    std::cout<<"Using Sub-lattice Decomposition! (Experimental Version)"<<std::endl;
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nxSL",1));
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nySL",1));
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nzSL",1));
    std::vector<int> ncxyz = {1, 1, 1};
    for(int i=0; i<3; i++) {
      if(nxyzSL[i]%2 != 0) nxyzSL[i]=(nxyzSL[i]/2)*2;
      if(nxyzSL[i]<1) nxyzSL[i]=1;
      if(nxyzSL[i]>1) ncxyz[i] =2;
    }
    ntrajectories = 0;
    for(int kk=0; kk<nxyzSL[2]; kk++) {
      for(int jj=0; jj<nxyzSL[1]; jj++) {
        for(int ii=0; ii<nxyzSL[0]; ii++) {
          int thiscolor = (kk%ncxyz[2])*ncxyz[2]*ncxyz[1] +
          (jj%ncxyz[1])*ncxyz[1] +
          (ii%ncxyz[0]);
          SLDTrajectory.push_back(Trajectory());
          SLDTrajectory[ntrajectories].color = thiscolor;
          std::vector<int> xyzSL = {ii,jj,kk};
          officialSyncStatus.push_back(SyncStatus(ntrajectories,nxyzSL,xyzSL));
          sldlabels.push_back(0);
          sldtrans.push_back(0);
          ntrajectories++;
        }
      }
    }
    ncolors = ncxyz[0]*ncxyz[1]*ncxyz[2];
  }
  std::cout<<"Using ntrajectories = "<<ntrajectories
  <<" and ncolors = "<<ncolors<<std::endl;
  
  for(int i=0; i<ntrajectories; i++) {
    usedBlocksSD.push_back(0);
    if(!useSubLattice) {
      SLDTrajectory.push_back(Trajectory());
      SLDTrajectory[i].color = i % ncolors;
      officialSyncStatus.push_back(SyncStatus(ntrajectories,{1,1,1},{0,0,0}));
      sldlabels.push_back(0);
      sldtrans.push_back(0);
    }
    SLDTrajectory[i].blocksThisCycle = 0;
    SLDTrajectory[i].nKMCPred = 0;
  }
  
  //check if checkpoint files are present
  bool restartFromCheckPoint= boost::filesystem::exists("./traj.out.chk") && boost::filesystem::exists("./times.out.chk") && boost::filesystem::exists("./Splicer.chk");
  if(restartFromCheckPoint) {
    boost::filesystem::copy_file("./traj.out.chk","./traj.out",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out.chk","./times.out",boost::filesystem::copy_option::overwrite_if_exists);
    outTraj.open("./traj.out", std::ios::app);
    outTime.open("./times.out", std::ios::app);
  }
  else{
    outTraj.open("./traj.out", std::ios::out);
    outTime.open("./times.out", std::ios::out);
  }
  if(useSubLattice){
    // TODO: Add a trajSL.out.chk etc..
    outTrajSL.open("./trajSL.out", std::ios::out);
  }
  
  currentcolor = 0;
  currentTraj  = 0;
  minPerCycle = config.get<unsigned>("ParSplice.MultiTrajectories.minPerCycle",10);
  if(minPerCycle<1) minPerCycle=1;
  
  //initialize from config data
  wMin=config.get<double>("ParSplice.MC.MinimumTrajectoryWeigth",1e-12);
  defaultVisitDuration=config.get<unsigned>("ParSplice.MC.DefaultVisitDuration",100000000);
  predictionGranularity=config.get<unsigned>("ParSplice.MC.PredictionGranularity",1);
  batchSize=config.get<unsigned>("ParSplice.Topology.NWorkers",1);
  predictionTime=config.get<unsigned>("ParSplice.Topology.NWorkers",1);
  maxTaskMesgSize=config.get<int>("ParSplice.MaximumTaskMesgSize",1000000);
  validatorTimeout=config.get<unsigned>("ParSplice.Splicer.ValidatorTimeout",1000000);
  // Should always include the current head of ALL trajectories being spliced.
  // So... Ensure that: batchSize >= ntrajectories
  // (For SL, may only need to include the head of all trajectories for the
  // current "color" ??)
  if(batchSize < (ntrajectories / ncolors))
    batchSize = (ntrajectories / ncolors);
  // Dont try to predict too far into the future if you are using multiple
  // trajectories...
  predictionTime = predictionTime / (ntrajectories / ncolors);
  if(predictionTime < 1) predictionTime = 1;
  broadcastDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.BroadcastDelay",1) );
  reportDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.ReportDelay",10000) );
  checkpointDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.CheckpointDelay",100000) );
  flushOnModify= config.get<bool>("ParSplice.Splicer.FlushOnModify",0);
  runTime=std::chrono::minutes( config.get<unsigned>("ParSplice.RunTime",1000000) );
  
  //read task parameters
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.TaskParameters")) {
    std::string stype=v.second.get<std::string>("Type");
    boost::trim(stype);
    int type=taskTypeByLabel.at(stype);
    int flavor=v.second.get<int>("Flavor");
    BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("")) {
      std::string key=vv.first;
      std::string data=vv.second.data();
      boost::trim(key);
      boost::trim(data);
      std::cout<<key<<" "<<data<<std::endl;
      taskParameters[std::make_pair(type,flavor)][key]=data;
    }
  }
  
  driver=new DriverHandleType(workerComm);
  
  //TODO: FIX ME
  defaultFlavor=1;
  STATE_DBKEY=0;
  batchId=1;
  broadcastRequests=std::vector<MPI_Request>(children.size(),MPI_REQUEST_NULL);
  
  if(restartFromCheckPoint) {
    // create and open an archive for input
    std::ifstream ifs("Splicer.chk");
    boost::archive::text_iarchive ia(ifs);
    // read class state from archive
    ia >> *this;
    // archive and stream closed when destructors are called
  }
  else{
    for(int itrajectory = 0; itrajectory<ntrajectories; itrajectory++) {
      addsystem_byfile(config, itrajectory);
    }
  }
  for(int i=0; i<ntrajectories; i++) {
    std::cout<<"INITIAL TRAJECTORY "<<i<<": "<<std::endl;
    SLDTrajectory[i].print();
    sldlabels[i] = SLDTrajectory[i].back().label;
  }
  
};

SLParSpliceSplicer::~SLParSpliceSplicer(){
  delete driver;
};

void SLParSpliceSplicer::addsystem_byfile(boost::property_tree::ptree &config, int itrajectory){
  
  TaskType task;
  
  if(useSubLattice) {
    std::vector<std::string> sldfile_list;
    std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
    boost::trim(initialConfiguration);
    sldfile_list.push_back(initialConfiguration);
    sldfile_list.push_back(boost::lexical_cast<std::string>(itrajectory));
    std::string initialConfigurationSLD = boost::algorithm::join(sldfile_list, ".");
    
    task.type=PARSPLICE_TASK_INIT_FROM_FILE;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,defaultFlavor)];
    task.parameters["Filename"]=initialConfigurationSLD;
    driver->assign(task);
    while(not driver->probe(task)) {};
    task.parameters.clear();
    
  }else{
    std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
    boost::trim(initialConfiguration);
    
    task.type=PARSPLICE_TASK_INIT_FROM_FILE;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,defaultFlavor)];
    task.parameters["Filename"]=initialConfiguration;
    driver->assign(task);
    while(not driver->probe(task)) {};
    task.parameters.clear();
  }
  
  //// Need to set system.nmove (and reorder id's) if we are doing SL-ParSplice
  //std::cout<<" SLDEBUG LABEL (0) -> "<<task.systems[0].label<<std::endl;
  if(useSubLattice){
    task.type=PARSPLICE_TASK_MOVE_FIRST;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MOVE_FIRST,defaultFlavor)];
    driver->assign(task);
    while(not driver->probe(task)) {};
  }
  //std::cout<<" SLDEBUG LABEL (1) -> "<<task.systems[0].label<<std::endl;
  
  task.type=PARSPLICE_TASK_MIN;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  task.systems[0].type=TYPE::MINIMUM;
  
  if(useSubLattice){
    task.type=PARSPLICE_TASK_MOVE_FIRST;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MOVE_FIRST,defaultFlavor)];
    driver->assign(task);
    while(not driver->probe(task)) {};
  }else{
    task.type=PARSPLICE_TASK_LABEL;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_LABEL,defaultFlavor)];
    driver->assign(task);
    while(not driver->probe(task)) {};
  }
  //std::cout<<" SLDEBUG LABEL (2) -> "<<task.systems[0].label<<std::endl;
  
  task.type=PARSPLICE_TASK_REMAP;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  
  Rd data;
  pack(data,task.systems[0]);
  minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
  std::cout<<"DB STORE "<<data.size()<<" "<<task.systems[0].label<<std::endl;
  
  Visit v;
  v.label=task.systems[0].label;
  v.duration=0;
  SLDTrajectory[itrajectory].appendVisit(v);
  SLDTrajectory[itrajectory].flavor=defaultFlavor;
  
};

void SLParSpliceSplicer::processRecv(){
  //did we receive new completed tasks?
  
  if(die) {
    std::cout<<"Splicer: cancelling pending receives"<<std::endl;
    MPI_Cancel(&incomingRequest);
    MPI_Wait(&incomingRequest,&recvStatus);
  }
  else{
    
    if(not recvCompleted) {
      MPI_Test(&incomingRequest,&recvCompleted,&recvStatus);
    }
    if(recvCompleted) {
      Timer t;
      //extract
      int peer=recvStatus.MPI_SOURCE;
      int tag=recvStatus.MPI_TAG;
      int count=0;
      MPI_Get_count( &recvStatus, MPI_BYTE, &count );
      
      switch (tag) {
        case PARSPLICE_STATS_SEGMENTS_TAG: {
          
          std::list<SegmentDatabase> incomingBatches;
          TransitionStatistics statistics;
          unpack(rbuf,statistics,incomingBatches,std::size_t(count));
          
          for(auto itbatch=incomingBatches.begin(); itbatch!=incomingBatches.end(); itbatch++) {
            for(auto itdb=itbatch->db.begin(); itdb!=itbatch->db.end(); itdb++) {
              for(auto ittraj=itdb->second.begin(); ittraj!=itdb->second.end(); ittraj++) {
                producedBlocks+=ittraj->duration();
              }
            }
          }
          
          //validate the new batches
          for(auto it=incomingBatches.begin(); it!=incomingBatches.end(); it++) {
            //std::cout<<"SPLICER VALIDATED SEGMENTS "<<std::endl;
            //it->print();
            validator.validate(peer,*it);
          }
          
          //transfer to segmentDB
          while(true) {
            validator.refreshHead(validatorTimeout);
            SegmentDatabase validatedSegments=validator.release();
            if(validatedSegments.size()>0) {
              //std::cout<<"SPLICER MERGING SEGMENTS "<<std::endl;
              //validatedSegments.print();
              segmentDB.merge(validatedSegments);
            }
            else{
              break;
            }
          }
          
          //update Markov model
          markovModel.assimilate(statistics);
          break;
        }
          
        case PARSPLICE_SYNC_STATS_TAG: {
          
          std::cout<<"SPLICER RECVING SYNC RESULT "<<std::endl;
          
          SyncStatistics syncstatistics;
          unpack(rbuf,syncstatistics);
          
          std::cout<<"SPLICER UNPACKED SYNC RESULT "<<std::endl;
          
          // DO SOMETHING HERE WITH THE SYNC STATISTICS!!
          // (i.e. update status of the "waiting" trajectories)
          std::map<unsigned, SyncData> syncMap = syncstatistics.syncMap;
          for(auto it=syncMap.begin(); it!=syncMap.end(); it++) {
            
            std::cout<<"SPLICER ITERATING IN SYNC RESULT "<<std::endl;
            
            unsigned itrajectory = it->first;
            SyncData sd = it->second;
            
            std::cout<<"SPLICER  itrajectory = "<<itrajectory<<std::endl;
            std::cout<<"SPLICER  SyncData.batchId = "<<sd.batchId<<std::endl;
            std::cout<<"SPLICER  SyncData.label = "<<sd.label<<std::endl;
            
            if(officialSyncStatus[itrajectory].batchId==sd.batchId) {
              Label oldlabel = SLDTrajectory[itrajectory].back().label;
              Label newlabel = sd.label;
              std::cout<<"SYNC "<<sd.batchId<<" of itrajectory "<<itrajectory
              <<" -> old label = "<<oldlabel
              <<" -> new label = "<<newlabel<<std::endl;
              
              // Update sldlabels if there was a sync:
              sldlabels[itrajectory] = newlabel;
              
              //TODO: Check if it is this simple:
              Visit v;
              v.label=sd.label;
              v.duration=0;
              SLDTrajectory[itrajectory].appendVisit(v);
              //SLDTrajectory[itrajectory].back().label=sd.label;
              
              officialSyncStatus[itrajectory].clear();
              if(oldlabel!=newlabel) {
                // Treat like there was a transition here...
                SLDTrajectory[itrajectory].predm.clear();
                SLDTrajectory[itrajectory].nKMCPred=0;
              }
            }else{
              std::cout<<"Sync already done for itrajectory "<<itrajectory
              <<" and batchId "<<sd.batchId<<std::endl;
            }
          }
          std::cout<<"SPLICER DONE PROCESSING SYNC RESULT."<<std::endl;
          break;
        }
          
        default: {
          //We don't support any other task yet
          assert(tag==PARSPLICE_STATS_SEGMENTS_TAG or tag==PARSPLICE_SYNC_STATS_TAG);
          break;
        }
          
      }
      
      //post a new receive for tasks
      MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,splicerComm,&incomingRequest);
      recvCompleted=0;
      
    }
  }
};

void SLParSpliceSplicer::sendSyncBatch(std::set<unsigned> syncset){
  
  Timer t;
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server broadcasting ";
  std::cout<<"SPLICER BROADCASTING BCAST BATCH"<<std::endl;
  std::cout<<"CURRENT HEAD(S): "<<std::endl;
  for(int i=0; i<ntrajectories; i++) {
    std::cout<<SLDTrajectory[i].back().label <<std::endl;
  }
  
  //reset the send buffer
  std::vector<std::vector<char> > sBuf(children.size());
  
  int nTask = syncset.size();
  
  //split the load between children
  //TODO: This has to be improved to consider affinity
  unsigned nTaskPerChild=(children.size()>0 ? nTask/children.size() : 0);
  unsigned np=nTask%children.size();
  
  //pack and send the messages
  int iChild=0;
  for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
    
    //create the task bundle
    TaskDescriptorBundle taskBundle;
    TaskDescriptor task;
    task.type=PARSPLICE_TASK_SYNC_SL;
    task.flavor=defaultFlavor;
    task.batchId=batchId;
    unsigned n=0;
    unsigned nt=nTaskPerChild+(iChild<np ? 1 : 0);
    
    for(auto its=syncset.begin(); its!=syncset.end(); its++) {
      
      Label lb = SLDTrajectory[*its].back().label;
      std::cout<<"SCHEDULING SYNC OF STATE " <<lb<<" FOR WORKER "<<*it<<std::endl;
      
      task.nInstances=1;
      task.itrajectory=*its;
      task.systems.clear();
      SystemPlaceholder s;
      s.label=lb;         // Original state label for the SLD (trajectory) [to be the 0th system in the task]
      s.necessity=NECESSITY::REQUIRED;
      s.type=TYPE::MINIMUM;
      task.systems.push_back(s);
      
      // Now we need to add the systems that make up the "changed" neighbors:
      SyncStatus oSs = officialSyncStatus[*its];
      for(auto itc=oSs.changelist.begin(); itc!=oSs.changelist.end(); itc++) {
        // Note: "*itc" is a relation vector (ex. (1, 0 , -1)) of the
        // trajectory's neighbor, and "neighlabels" translates this vector to
        // the new state 'label' that the neighbor has transitioned to
        // .. "neighlabelsI" is the initial label for the neighbor
        
        // Original label of neighbor 1st
        s.label = oSs.neighlabelsI[*itc];
        s.necessity=NECESSITY::REQUIRED;
        s.type=TYPE::MINIMUM;
        task.systems.push_back(s);
        
        // Changed label of neighbor 2nd
        s.label = oSs.neighlabels[*itc];
        s.necessity=NECESSITY::REQUIRED;
        s.type=TYPE::MINIMUM;
        task.systems.push_back(s);
      }
      // Should also package simple info about the neighbors:
      task.changelist = oSs.changelist;
      taskBundle.insert(task);
    }
    pack(sBuf[iChild],taskBundle);
    //std::cout<<splicerComm<<" "<<&(sBuf[iChild][0])<<" "<<sBuf[iChild].size()<<" "<<*it<<" "<<PARSPLICE_TASK_TAG <<" "<< &(broadcastRequests[iChild])<<std::endl;
    
    //TODO make this more robust
    assert(sBuf[iChild].size()<maxTaskMesgSize);
    MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
    
    break; // Only send the synchronization task to the 0th child (for now)
  }
  
  lastBroadcast=std::chrono::high_resolution_clock::now();
  std::cout<<"SPLICER DONE BROADCASTING SYNC BATCH "<<t.stop()<<std::endl;
  batchId++;
};

void SLParSpliceSplicer::processSend(){
  
  //test for completion of the previous task broadcast
  MPI_Testall(children.size(), &(broadcastRequests[0]),&broadcastCompleted, MPI_STATUSES_IGNORE);
  
  if(die and not killed) {
    std::cout<<"Splicer: killing the workers"<<std::endl;
    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);
    
    TaskDescriptorBundle taskBundle;
    TaskDescriptor task;
    task.type=PARSPLICE_TASK_DIE;
    task.flavor=defaultFlavor;
    task.batchId=batchId;
    task.nInstances=1;
    task.systems.clear();
    taskBundle.insert(task);
    
    int iChild=0;
    for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
      pack(sBuf[iChild],taskBundle);
      //TODO make this more robust
      assert(sBuf[iChild].size()<maxTaskMesgSize);
      MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
    }
    killed=true;
    
    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);
  }
  
  //buffer the predictions and broadcast only if delay has passed since the last time
  //also make sure we are done broadcasting the previous batch
  if(std::chrono::high_resolution_clock::now() - lastBroadcast> broadcastDelay and broadcastCompleted) {
    
    //rjz// First, check if we have any syncronization tasks to do:
    std::set<unsigned> syncset;
    if(useSubLattice && (ntrajectories>1)) {
      for(int i=0; i<ntrajectories; i++) {
        if(SLDTrajectory[i].color!=currentcolor) continue;
        if((officialSyncStatus[i].length>0)&&(officialSyncStatus[i].batchId==0)) {
          syncset.insert(i);
          officialSyncStatus[i].batchId=batchId;
          std::cout<<"NEED SYNC: itrajectory = "<<i
          <<" - batchId = "<<batchId<<std::endl;
        }
      }
      if(syncset.size()>0) {
        sendSyncBatch(syncset);
        return;
        // Don't change 'lastBroadcast' so other trajectories can be assigned
        // (traj's that dont need to be synced)?
      }
    }
    
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server broadcasting ";
    std::cout<<"SPLICER BROADCASTING"<<std::endl;
    std::cout<<"CURRENT HEAD(S): "<<std::endl;
    for(int i=0; i<ntrajectories; i++) {
      std::cout<<SLDTrajectory[i].back().label <<std::endl;
    }
    
    //reset the send buffer
    sBuf=std::vector<std::vector<char> >(children.size());
    
    {
      
      std::map<Label, unsigned > counts;
      int nKeep=0;
      
      //TODO: make this faster
      
      // Step 1. Combine all SLDTrajectory.predm data into an
      //         aggpredm object.. (aggregate predictions):
      std::unordered_map<Label, std::multiset<unsigned> > aggpredm;
      for(int i=0; i<ntrajectories; i++) {
        if(SLDTrajectory[i].color != currentcolor) continue;
        if(useSubLattice && (officialSyncStatus[i].length > 0))
          continue; // This trajectory needs to be synchronized...
        
        //// Make sure we have the head of each trajectory in 'counts'
        //if(ntrajectories > 1){
        //  counts[SLDTrajectory[i].back().label]+=1;
        //  nKeep++;
        //}
        
        // Loop through the predm entires (the label-multiset pairs in predm)
        for(auto it=SLDTrajectory[i].predm.begin();
            it!=SLDTrajectory[i].predm.end(); it++) {
          // Need to loop through the items in each multiset
          for(auto itm=(it->second).begin(); itm!=(it->second).end(); itm++) {
            aggpredm[it->first].insert(*itm);
          }
        }
      }
      
      // Step 2. Populate schedulingSet using aggpredm data:
      std::set<SchedulerState> schedulingSet;
      for(auto it=aggpredm.begin(); it!=aggpredm.end(); it++) {
        it->second.erase(0);
        SchedulerState s;
        s.label=it->first;
        s.counts=it->second;
        if(s.counts.size()>0) {
          schedulingSet.insert(s);
        }
      }
      if(1) { // Debug option to print the schedulingSet information
        std::cout<<" schedulingSet ----> "<<std::endl<<std::flush;
        for(auto it=schedulingSet.begin(); it!=schedulingSet.end(); it++) {
          std::cout<<it->label<<" "<<it->counts.size()<<std::endl<<std::flush;
        }
      }
      
      
      while(nKeep<batchSize and schedulingSet.size()>0) {
        //find the state with the largest number of KMC instances that required more segments
        auto it=schedulingSet.rbegin();
        
        SchedulerState s=*it;
        //remove from the set
        schedulingSet.erase(std::next(it).base());
        for(int i=0; i<predictionGranularity; i++) {
          nKeep++;
          counts[s.label]+=1;
          s.counts.erase(counts[s.label]);
        }
        //reinsert if there are KMC instances left
        if(s.counts.size()>0) {
          //hint that the updated state might still be close to the back
          schedulingSet.insert(schedulingSet.cend(),s);
        }
      }
      
      //clear the previous predictions
      for(int i=0; i<ntrajectories; i++) {
        if(SLDTrajectory[i].color != currentcolor) continue;
        if(useSubLattice && (officialSyncStatus[i].length > 0)) continue; // This trajectory needs to be synchronized...
        SLDTrajectory[i].predm.clear();
        SLDTrajectory[i].nKMCPred=0;
      }
      
      //split the load between children
      //TODO: This has to be improved to consider affinity
      unsigned nTaskPerChild=(children.size()>0 ? nKeep/children.size() : 0);
      unsigned np=nKeep%children.size();
      
      //pack and send the messages
      int iChild=0;
      for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
        
        
        
        //create the task bundle
        TaskDescriptorBundle taskBundle;
        TaskDescriptor task;
        task.type=PARSPLICE_TASK_SEGMENT;
        task.flavor=defaultFlavor;
        task.batchId=batchId;
        unsigned n=0;
        unsigned nt=nTaskPerChild+(iChild<np ? 1 : 0);
        for(auto itc=counts.begin(); itc!=counts.end(); ) {
          unsigned nInstances=itc->second;
          Label lb=itc->first;
          
          //we can take in all of these tasks
          if(n+nInstances<nt) {
            n+=nInstances;
            itc=counts.erase(itc);
          }
          //we can only take part of these tasks
          else{
            nInstances=nt-n;
            n=nt;
            itc->second-=nInstances;
          }
          
          std::cout<<"SCHEDULING "<<nInstances<<" INSTANCES IN STATE " <<lb<<" FOR WORKER "<<*it<<std::endl;
          
          task.nInstances=nInstances;
          task.systems.clear();
          SystemPlaceholder s;
          s.label=lb;
          s.necessity=NECESSITY::REQUIRED;
          s.type=TYPE::MINIMUM;
          task.systems.push_back(s);
          s.necessity=NECESSITY::OPTIONAL;
          s.type=TYPE::QSD;
          task.systems.push_back(s);
          
          taskBundle.insert(task);
          
          if(n==nt) {
            break;
          }
        }
        pack(sBuf[iChild],taskBundle);
        
        //TODO make this more robust
        assert(sBuf[iChild].size()<maxTaskMesgSize);
        MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
      }
    }
    lastBroadcast=std::chrono::high_resolution_clock::now();
    
    std::cout<<"SPLICER DONE BROADCASTING "<<t.stop()<<std::endl;
    batchId++;
  }
};


void SLParSpliceSplicer::spliceSLD(int itrajectory){
  Timer t;
  unsigned long usedBefore;
  
  // Check for early return:
  if(SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle) return;
  
  Label previousLabel=SLDTrajectory[itrajectory].back().label;
  
  bool spliced=false;
  while(usedBefore=usedBlocksSD[itrajectory], spliceOne(itrajectory)) {
    spliced=true;
    if(modifier.modificationNeeded(usedBefore,usedBlocksSD[itrajectory]))
    {
      std::cout<<"MODIFY "<<SLDTrajectory[itrajectory].back().label<<std::endl;
      Rd data;
      SystemType s;
      uint64_t res=minimaStore->reserve(STATE_DBKEY,SLDTrajectory[itrajectory].back().label);
      while(not minimaStore->get(STATE_DBKEY, SLDTrajectory[itrajectory].back().label,data)) {minimaStore->singleServe();}
      minimaStore->release(res);
      unpack(data,s,0);
      
      modifier.modify(s, usedBefore, usedBlocksSD[itrajectory]);
      
      std::cout<<"MODIFY DONE"<<std::endl;
      TaskType task;
      task.systems.push_back(s);
      
      task.type=PARSPLICE_TASK_MIN;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      std::cout<<"MIN DONE"<<std::endl;
      
      task.type=PARSPLICE_TASK_REMAP;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      std::cout<<"REMAP DONE "<<task.systems[0].label<<std::endl;
      
      data.clear();
      pack(data,task.systems[0]);
      minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
      std::cout<<"PUT DONE"<<std::endl;
      
      Visit v;
      v.label=task.systems[0].label;
      v.duration=0;
      SLDTrajectory[itrajectory].appendVisit(v);
      std::cout<<"APPEND DONE"<<std::endl;
      
      if(flushOnModify) {
        markovModel.clear();
        segmentDB.clear();
      }
    }
    if((ncolors>1) &&
       (SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle)) break;
  }
  Label currentLabel=SLDTrajectory[itrajectory].back().label;
  if(currentLabel!=previousLabel) {
    SLDTrajectory[itrajectory].predm.clear();
    SLDTrajectory[itrajectory].nKMCPred=0;
    // Add synchronization item when color is finished...
    sldtrans[itrajectory]=1;
  }
  if(spliced) {
    //std::cout<<"DONE SPLICING: "<<t.stop()<<std::endl;
  }
  
};

void SLParSpliceSplicer::splice(){
  
  //Splice trajectories in simple order for now
  //For SL: will cycle trhough a specific "color"
  //        and will only allow a limited number of segments to be spliced.
  bool colordone = 1;
  for(int i=0; i<ntrajectories; i++) {
    int iuse = (i + currentTraj) % ntrajectories;
    if(SLDTrajectory[iuse].color == currentcolor) {
      
      if(useSubLattice && (officialSyncStatus[iuse].length > 0)){
        colordone = 0;
        continue; // This trajectory needs to be synchronized...
      }
      
      spliceSLD(iuse);
      if(SLDTrajectory[iuse].blocksThisCycle < minPerCycle) {
        colordone = 0;
      }
    }
  }
  currentTraj = (currentTraj + 1) % ntrajectories;
  if(colordone) {
    for(int i=0; i<ntrajectories; i++) {
      if(SLDTrajectory[i].color == currentcolor) {
        SLDTrajectory[i].blocksThisCycle -= minPerCycle;
      }
    }
    
    // SLD 'Update' and add item to trajSLlist:
    std::vector<Label> newitem;
    for(int i=0; i<ntrajectories; i++){
      if(SLDTrajectory[i].color == currentcolor) {
        // Determine label before the 'color' was finished:
        Label oldLabel = sldlabels[i];
        // Determine label after the 'color' was finished:
        Label newLabel = SLDTrajectory[i].back().label;
        // Push value for trajSL.out:
        newitem.push_back(oldLabel);
        // Update the 'old' label for next time around:
        sldlabels[i] = newLabel;
        // Need to schedule sync tasks for neighbors if
        // the label changed from a transition (not from a sync):
        if((oldLabel != newLabel) && (sldtrans[i])){
          sldtrans[i]=0;
          // Update 'officialSyncStatus'
          if(useSubLattice && (ntrajectories>1)) {
            std::map<std::vector<int>, unsigned> sln = officialSyncStatus[i].SLneighs;
            for(auto it=sln.begin(); it!=sln.end(); it++) {
              std::cout<<"ADD CHANGE in itrajectory "<<i<<" TO jtrajectory: "<<it->second<<std::endl;
              std::cout<<"ADD CHANGE, vector: "<<it->first[0]<<","<<it->first[1]<<","<<it->first[2]<<std::endl;
              officialSyncStatus[it->second].addnew(i,oldLabel,newLabel);
              //std::cout<<"DONE ADDING CHANGE."<<std::endl;
              officialSyncStatus[it->second].print(it->second);
            }
          }
        }
        
      }else{
        newitem.push_back(SLDTrajectory[i].back().label);
      }
    }
    if(useSubLattice && (currentcolor==0)) trajSLlist.push_back(newitem);
    
    currentcolor += 1;
    if(currentcolor == ncolors) currentcolor = 0;
    std::cout<<" -- CURRENT COLOR = "<<currentcolor<<std::endl;
    
  }
  
};

void SLParSpliceSplicer::KMC(){
  for(int itrajectory=0; itrajectory<ntrajectories; itrajectory++) {
    if(SLDTrajectory[itrajectory].color == currentcolor) {
      if(SLDTrajectory[itrajectory].nKMCPred<100) {
        //generate a virtual trajectory
        predictions.clear();
        KMCSchedule(predictions,itrajectory);
        //std::cout<<"CURRENT HEAD "<< SLDTrajectory.back().label <<std::endl;
        for(auto it=predictions.begin(); it!=predictions.end(); it++) {
          //std::cout<<"PREDICTION: "<<it->first<<" "<<it->second<<std::endl;
          SLDTrajectory[itrajectory].predm[it->first].insert(it->second);
        }
        //std::cout<<"DONE KMC: "<<t.stop()<<std::endl;
      }
      SLDTrajectory[itrajectory].nKMCPred++;
    }
  }
};

void SLParSpliceSplicer::writeSL(){
  // Loop through items in trajSLlist
  while(trajSLlist.size()>0){
    std::vector<Label> labels = trajSLlist.front();
    std::stringstream ss;
    // write the global time:
    //ss<<(time);
    // Write statel labels
    for(std::vector<Label>::iterator it=labels.begin(); it!=labels.end(); it++) {
      ss<<" "<<(*it);
    }
    ss<<"\n";
    outTrajSL<<ss.str();
    outTrajSL.flush();
    trajSLlist.pop_front();
  }
}

void SLParSpliceSplicer::report(){
  //report at given intervals
  if(std::chrono::high_resolution_clock::now() - lastReport> reportDelay  ) {
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting ";
    
    //output timings
    outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;
    
    // Update trajSL.out (if applicable):
    if(useSubLattice) writeSL();
    
    for(int itrajectory=0; itrajectory<ntrajectories; itrajectory++) {
      
      //output trajectory
      int iVisit=0;
      Visit back=SLDTrajectory[itrajectory].back();
      SLDTrajectory[itrajectory].pop_back();
      
      std::stringstream ss;
      for(auto it=SLDTrajectory[itrajectory].visits.begin(); it!=SLDTrajectory[itrajectory].visits.end(); it++) {
        ss<<" "<<it->label<<" "<<it->duration<<" "<<itrajectory<<"\n";
        iVisit++;
      }
      outTraj<<ss.str();
      outTraj.flush();
      
      SLDTrajectory[itrajectory].clear();
      SLDTrajectory[itrajectory].appendVisit(back);
      
    }
    
    lastReport=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting done ";
    std::cout<<"SPLICER DONE REPORTING "<<t.stop()<<std::endl;
  }
};

void SLParSpliceSplicer::checkpoint(bool now){
  //checkpoint at given intervals
  if(now or std::chrono::high_resolution_clock::now() - lastCheckpoint> checkpointDelay  ) {
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing ";
    //checkpoint();
    outTraj.close();
    outTime.close();
    
    //copy consistent progress files
    boost::filesystem::copy_file("./traj.out","./traj.out.chk",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out","./times.out.chk",boost::filesystem::copy_option::overwrite_if_exists);
    
    outTraj.open("./traj.out", std::ios::app | std::ios::out);
    outTime.open("./times.out", std::ios::app | std::ios::out);
    
    
    std::ofstream ofs("./Splicer.chk");
    // save data to archive
    {
      boost::archive::text_oarchive oa(ofs);
      // write class instance to archive
      oa << *this;
      // archive and stream closed when destructors are called
    }
    
    std::cout<<"CHECKPOINTING"<<std::endl;
    lastCheckpoint=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing done";
  }
};


void SLParSpliceSplicer::KMCSchedule( std::unordered_map<Label, unsigned > & visits, int itrajectory ){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC ";
  
  
  //keep track of which segment in the database we already used
  std::unordered_map<Label, int> consumedSegments;
  
  //start at the current end
  Label c=SLDTrajectory[itrajectory].back().label;
  double w=1;
  unsigned length=0;
  std::pair<Label, unsigned> s;
  {
    //boost::timer::auto_cpu_timer t(3, "KMC scheduling loop: %w seconds\n");
    
    while(true) {
      //"virtually" consume segment from the database
      auto key=std::make_pair(c,defaultFlavor);
      
      if(segmentDB.count(c,defaultFlavor)>0 and consumedSegments[c]< segmentDB.count(c,defaultFlavor) ) {
        Trajectory &t=segmentDB.db[key][consumedSegments[c]];
        consumedSegments[c]+=1;
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC virtual consume "<<c<<" "<<t.back().label;
        //std::cout<<"Splicer KMC virtual consume "<<c<<" "<<t.back().label<<std::endl;
        c=t.back().label;
      }
      //generate segment with BKL
      else{
        s=markovModel.sampleBKL(c, defaultVisitDuration, w);
        //std::cout<<"#Splicer KMC generate "<<c<<" "<<s.first<<" "<<s.second<<" "<<w<<std::endl;
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC generate "<<c<<" "<<s.first<<" "<<s.second<<" "<<w;
        int duration=s.second;
        Visit v;
        v.label=c;
        //do not exceed the prediction horizon
        v.duration=( duration+length>predictionTime ? predictionTime-length : duration );
        length+=v.duration;
        //visits.push_back(v);
        visits[v.label]+=v.duration; // RJZ: Should we be using += if v.label is not in visits yet??
        c=s.first;
        //break if weight becomes too low or we reached the prediction time
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC length "<<length<<" "<<v.duration<<" "<<s.second;
        if(length>=predictionTime or w<wMin) {
          break;
        }
      }
    }
  }
};

bool SLParSpliceSplicer::spliceOne(int itrajectory){
  
  if(SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle)
    return false;
  
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer::splice ";
  //add segments to the official trajectory
  Label end=SLDTrajectory[itrajectory].back().label;
  //BOOST_LOG_SEV(lg,info) <<"#Splicer Splice Current end: "<<end;
  
  auto key=std::make_pair(end,defaultFlavor);
  
  if( segmentDB.count(end,defaultFlavor)> 0 ) {
    
    Trajectory t;
    
    if((ntrajectories > 1) && (ncolors > 1)) {
      
      // If splicing multiple trajectories, call 'front_split'...
      // Should only take one visit at a time, and need to split that visit
      // if it is too long
      int needblocks = minPerCycle;
      needblocks -= SLDTrajectory[itrajectory].blocksThisCycle;
      segmentDB.front_split(end,defaultFlavor,t,needblocks);
      
    }else{
      
      // Removed the entire trajectory at 'end'.. we can use everything
      segmentDB.front(end,defaultFlavor,t);
      segmentDB.pop_front(end,defaultFlavor);
      
    }
    
    usedBlocks+=t.duration();
    usedBlocksSD[itrajectory]+=t.duration();
    
    //rjz//
    SLDTrajectory[itrajectory].blocksThisCycle += t.duration();
    if(SLDTrajectory[itrajectory].blocksThisCycle >= (minPerCycle+20)) {
      std::cout<<"WARNING -- "<<SLDTrajectory[itrajectory].blocksThisCycle
      <<" blocks have been spliced for itrajectory "
      <<itrajectory<<" this cycle!!\n";
    }
    
    //unusedBlocks-=t.size();
    SLDTrajectory[itrajectory].splice(t);
    end=SLDTrajectory[itrajectory].back().label;
    key=std::make_pair(end,defaultFlavor);
    std::cout<<"#Splicer Splice Current end: "<<end<<" for itrajectory "<<itrajectory<<"\n";
    std::cout<<"itrajectory "<<itrajectory<<" now has blocksThisCycle = "
    <<SLDTrajectory[itrajectory].blocksThisCycle<<"\n";
    //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;
    return true;
  }
  else{
    return false;
  }
  
};

void SLParSpliceSplicer::output(){
  
  //output timings
  outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;
  
  // Update trajSL.out (if applicable):
  if(useSubLattice) writeSL();
  
  //loop through trajectories
  for(int itrajectory=0; itrajectory<ntrajectories; itrajectory++) {
    
    //output trajectory
    int iVisit=0;
    Visit back=SLDTrajectory[itrajectory].back();
    SLDTrajectory[itrajectory].pop_back();
    
    std::stringstream ss;
    for(auto it=SLDTrajectory[itrajectory].visits.begin(); it!=SLDTrajectory[itrajectory].visits.end(); it++) {
      ss<<" "<<it->label<<" "<<it->duration<<" "<<itrajectory<<"\n";
      iVisit++;
    }
    outTraj<<ss.str();
    outTraj.flush();
    
    SLDTrajectory[itrajectory].clear();
    SLDTrajectory[itrajectory].appendVisit(back);
    
  }
  
};

