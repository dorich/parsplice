
#include "Types.hpp"

////////////////////////////////////////////////////////////////////////////////////////////

SyncStatistics::SyncStatistics(){};

void SyncStatistics::update(Label finalLabel, unsigned itrajectory, uint64_t batchId){
	SyncData sd;
	sd.label = finalLabel;
	sd.batchId = batchId;
	syncMap[itrajectory] = sd;
};

void SyncStatistics::clear(){
	syncMap.clear();
};

int SyncStatistics::size(){
	return syncMap.size();
};

////////////////////////////////////////////////////////////////////////////////////////////

StateStatistics::StateStatistics(){
	nSegments=0;
	nTransitions=0;
};

StateStatistics::StateStatistics(uint64_t label_){
	nSegments=0;
	nTransitions=0;
	label=label_;
};

void StateStatistics::update(uint64_t finalState){
	nSegments++;
	if(finalState!=label) {
		counts[finalState]+=1;
		nTransitions+=1;
	}
};

void StateStatistics::update(StateStatistics s){
	nSegments+=s.nSegments;
	nTransitions+=s.nTransitions;
	for(auto it=s.counts.begin(); it!=s.counts.end(); it++) {
		counts[it->first]+=s.counts[it->first];
	}
};


//sample a final state
std::pair<Label,unsigned> StateStatistics::sampleBKL(unsigned defaultDuration, double &w, boost::random::mt11213b &rand, boost::random::uniform_01<> &uniform){

	//this state has no known transition
	if(nTransitions==0) {
		w*=0;
		return std::make_pair(label,defaultDuration);
	}

	double r1=uniform(rand);
	double r2=2.;
	Label final;

	double rN=r1*nTransitions;
	double rAccum=0.;
	for(auto it=counts.begin(); it!=counts.end(); it++) {
		rAccum+=it->second;
		if(rAccum>=rN) {
			final=it->first;
			break;
		}
	}

	//update the weigth of the trajectory
	w*=double(nTransitions-0.5)/nTransitions;

	//duration. We do not sample, but use a "safe" value.
	unsigned duration=r2*unsigned(nSegments/double(nTransitions));

	return std::make_pair(final,duration);
};


Label StateStatistics::sampleSegmentBKL(boost::random::mt11213b &rand, boost::random::uniform_01<> &uniform){

	double r1=uniform(rand);
	Label final=label;

	double rN=r1*nSegments;
	double rAccum=0.;
	for(auto it=counts.begin(); it!=counts.end(); it++) {
		rAccum+=it->second;
		if(rAccum>=rN) {
			final=it->first;
			break;
		}
	}

	return final;
};


void StateStatistics::clear(){
	nSegments=0;
	counts.clear();
};

////////////////////////////////////////////////////////////////////////////////////////////

TransitionStatistics::TransitionStatistics(){
	boost::random::random_device rd;
	rng.seed(rd());
};

void TransitionStatistics::clear(){
	statistics.clear();
};

void TransitionStatistics::update(uint64_t initialState, uint64_t finalState){

	if(statistics.count(initialState)==0) {
		StateStatistics s(initialState);
		statistics.insert(std::make_pair(initialState,s));
	}
	statistics.find(initialState)->second.update(finalState);
};

void TransitionStatistics::assimilate(TransitionStatistics &s){
	for(auto it=s.statistics.begin(); it!=s.statistics.end(); it++) {
		if(statistics.count(it->first)==0) {
			StateStatistics s(it->first);
			statistics.insert(std::make_pair(it->first,s));
		}
		statistics.find(it->first)->second.update(it->second);
	}
	s.clear();
};


//sample a final state
std::pair<Label,unsigned> TransitionStatistics::sampleBKL(Label &lb, unsigned defaultDuration, double &w){
	//unknown state
	if( statistics.count(lb)==0 ) {
		w*=0;
		return std::make_pair(lb,defaultDuration);
	}
	return statistics.find(lb)->second.sampleBKL(defaultDuration,w,rng,uniform);
	//return statistics[lb].sampleBKL(defaultDuration,w,rng,uniform);
};

int TransitionStatistics::size(){
	return statistics.size();
};

////////////////////////////////////////////////////////////////////////////////////////////


Trajectory::Trajectory(){
	(*this).length=0;
};

void Trajectory::print(){
	std::cout<<"============"<<std::endl;

	for(auto it=visits.begin(); it!=visits.end(); it++) {
		std::cout<<it->label<<" "<<it->duration<<std::endl;
	}
};

void Trajectory::clear(){
	visits.clear();
	length=0;
	index=0;
};

//append a visit to a trajectory. Extends the last visit if possible
void Trajectory::appendVisit(Visit &v, bool extend){
	(*this).length+=v.duration;

	if( extend and !(*this).empty() and (*this).back().label==v.label) {
		//extend the last visit
		(*this).back().duration+=v.duration;
		return;
	}
	//new visit
	(*this).visits.push_back(v);
};


//splice two trajectories
bool Trajectory::splice(Trajectory &t){

	//do not splice empty trajectories
	if( (*this).empty() or t.empty() ) {
		return false;
	}

	//splice only if the beginning and end match
	if(  (*this).back().label == t.front().label ) {
		(*this).back().duration+=t.front().duration;
		t.visits.pop_front();
		(*this).visits.splice((*this).visits.end(), t.visits);
		(*this).length+=t.length;

		//consume t
		t.clear();
		return true;
	}
	return false;
};

//splice two trajectories
void Trajectory::reduce_length(int n){
  if((*this).length >= n){
    (*this).length -= n;
  }else{
    (*this).length = 0;
  }
};

/*
   uint64_t Trajectory::size(){
        return visits.size();
   };
 */

uint64_t Trajectory::duration(){
	return length;
};


bool Trajectory::empty(){
	return visits.size()==0;
};

Visit& Trajectory::back(){
	return visits.back();
};
Visit& Trajectory::front(){
	return visits.front();
};

void Trajectory::pop_back(){
	visits.pop_back();
};

void Trajectory::pop_front(){
	visits.pop_front();
};

////////////////////////////////////////////////////////////////////////////////////////////

void SegmentDatabase::clear(){
	db.clear();
};

int SegmentDatabase::size(){
	return db.size();
};

int SegmentDatabase::count(Label lb, int flavor){
	auto key=std::make_pair(lb,flavor);
	return db[key].size();
};

bool SegmentDatabase::front(Label lb, int flavor, Trajectory &t){
	if(count(lb,flavor)==0) {
		return false;
	}
	auto key=std::make_pair(lb,flavor);
	t=db[key].front();
	return true;
};

void SegmentDatabase::pop_front(Label lb, int flavor){
	if(count(lb,flavor)!=0) {
		auto key=std::make_pair(lb,flavor);
		db[key].pop_front();
	}
};

void SegmentDatabase::add(int flavor, Trajectory &t){
	if(not t.empty()) {
		Label lb=t.front().label;
		auto key=std::make_pair(lb,flavor);
		//if trajectory exist in that bin, try to splice at back. Otherwise, add
		if(db.count(key)>0) {
			if(db[key].empty() or (not db[key].back().splice(t))) {
				db[key].push_back(t);
			}
		}
		else{
			db[key]=std::deque<Trajectory>();
			db[key].push_back(t);
		}
	}
	else{
		std::cout<<"SegmentDatabase::add WARNING! Adding an empty trajectory"<<std::endl;
	}
};

void SegmentDatabase::front_split(Label lb, int flavor, Trajectory &t, int n){
  // Set t to be a trajectory of with one visit, or an 'n' block duration.
  // Remove this whole or partial visit from the database.
	if(count(lb,flavor)!=0) {
		auto key=std::make_pair(lb,flavor);
		//t_front = db[key].front();  // front traj, to be split
    t = Trajectory();           // portion of front traj to be returned

    std::cout<<"Splitting from t_front: "<<std::endl;
    db[key].front().print();

    if(db[key].front().front().duration > n){

      // split the visit
      Visit v;
      v.label = db[key].front().front().label;
      v.duration = n;
      db[key].front().front().duration -= n;
      db[key].front().reduce_length( n );
      t.appendVisit( v );
      std::cout<<"consuming "<<n
               <<" blocks of "<<lb<<std::endl;

    }else{

      // consume the whole visit
      t.appendVisit( db[key].front().front() );
      std::cout<<"consuming " << db[key].front().front().duration
               <<" blocks of " << lb << std::endl;
      db[key].front().pop_front();
      db[key].front().reduce_length( n );

      if(db[key].front().empty()){

        std::cout<<"POPPING FRONT."<<std::endl;
        pop_front(lb, flavor);

      }else{

        std::cout<<"t_front HAS A TRANSITION. "<<std::endl;
        t.appendVisit( db[key].front().front() );
        std::cout<<"consuming "<<db[key].front().front().duration
                 <<" blocks of "<<db[key].front().front().label<<std::endl;
        pop_front(lb, flavor);

      }
    }

    std::cout<<"RESULTING t: "<<std::endl;
    t.print();

	}
};

void SegmentDatabase::print(){
	for(auto it=db.begin(); it!=db.end(); it++) {
		std::cout<<it->first.first<<" "<<it->first.second<<std::endl;
		for(auto itt=it->second.begin(); itt!=it->second.end(); itt++) {
			itt->print();
		}
	}
};

void SegmentDatabase::merge(SegmentDatabase &supp){
	//loop over initial states
	for(auto it=supp.db.begin(); it!=supp.db.end(); it++) {
		//loop over trajectories
		int flavor=it->first.second;
		for(auto itt=it->second.begin(); itt!=it->second.end(); itt++) {
			Trajectory &t=*itt;
			this->add(flavor,t);
		}
	}
	supp.clear();
};
