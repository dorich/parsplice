/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#ifndef AbstractSystem_hpp
#define AbstractSystem_hpp

#include <stdio.h>
#include <cstdint>
#include <vector>
#include <map>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/map.hpp>


#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


enum class TYPE : int {MINIMUM, SADDLE, QSD, EMPTY, OTHER};
enum class NECESSITY : int {REQUIRED, OPTIONAL};

class AbstractSystem {
public:
virtual ~AbstractSystem(){
	necessity=NECESSITY::OPTIONAL;
	type=TYPE::EMPTY;
	label=0;
};
uint64_t label;
NECESSITY necessity;
TYPE type;

template<class Archive>
void serialize(Archive & ar, const unsigned int version){
	ar & label;
	ar & necessity;
	ar & type;
}

virtual void remap(std::map<int,int> newUniqueID){
};

};


class SystemPlaceholder : public AbstractSystem {
public:

bool operator ==(const SystemPlaceholder &b) const {
	return (label==b.label and type==b.type and necessity==b.necessity);
};

bool operator !=(const SystemPlaceholder &b) const {
	return !(*this==b);
};

template<class Archive>
void serialize(Archive & ar, const unsigned int version){
	ar & label;
	ar & necessity;
	ar & type;
}
};

namespace std
{
template<>
struct hash<SystemPlaceholder> {
	size_t operator()(const SystemPlaceholder &s) const {
		return std::hash<uint64_t>()(s.label) ^ std::hash<int>()(static_cast<int>(s.necessity)) ^ std::hash<int>()(static_cast<int>(s.type));
	}
};
}


/**
 * Abstract system base class. This is the object that will be stored and transfered by ParSplice.
 * Defines a minimal interface for all systems. Derived classes can provide additional members.
 *
 * A system should be able to store velocities, but these will not be modified from within ParSplice.
 */

class MinimalSystem : public AbstractSystem {
public:
virtual ~MinimalSystem(){
};
virtual int getNAtoms()=0;
virtual void setNAtoms(int n)=0;

virtual double getPosition(int iAtom, int iDim)=0;
virtual void setPosition(int iAtom, int iDim, double p)=0;

virtual int getSpecies(int iAtom)=0;
virtual void setSpecies(int iAtom, int species)=0;

virtual int getUniqueID(int iAtom)=0;

virtual void setBox(int iDim, int jDim, double b)=0;
virtual double getBox(int iDim, int jDim)=0;

virtual void setPeriodic(int iDim, bool p)=0;
virtual int getPeriodic(int iDim)=0;

virtual int addAtom(std::vector<double> positions, int species, std::map<std::string, std::string> attributes)=0;

private:
virtual void setUniqueID(int iAtom, int i)=0;

/**
 * Remap the unique IDs
 */
virtual void remap(std::map<int,int> newUniqueID){
	int nAtoms=getNAtoms();
	for(int i=0; i<nAtoms; i++) {
		setUniqueID(i, newUniqueID[ getUniqueID(i)]);
	}
};

};

#endif /* AbstractSystem_hpp */
