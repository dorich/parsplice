
#include "Validator.hpp"

/*
   void Validator::init(std::set<int> clients_,int batchSize_){
        clients=clients_;
        batchSize=batchSize_;

        ib=0;
        for(auto it=clients.begin(); it!=clients.end(); it++) {
                Placeholder p;
                p.producerID=*it;
                unvalidated.push_back(p);
        }

   };
 */

/*
   void Validator::validate(int peer, SegmentDatabase &batch){
        bool phFound=false;

        for(auto it=unvalidated.begin(); it!=unvalidated.end(); it++) {
                if(it->producerID==peer and it->segments.size()==0) {
                        //std::cout<<"VALIDATOR: PLACEHOLDER FOUND"<<std::endl;
                        it->segments=batch;
                        phFound=true;
                        break;
                }
        }

        //placeholder not found
        if(not phFound) {
                //std::cout<<"VALIDATOR: PLACEHOLDER NOT FOUND"<<std::endl;
                //BOOST_LOG_SEV(lg,warning) <<"#Validator::validate placeholder not found";
                //queue at the end
                Placeholder p;
                p.producerID=peer;
                p.segments=batch;
                unvalidated.push_back(p);
        }

        //insert new placeholder at the end
        {
                Placeholder p;
                p.producerID=peer;
                unvalidated.push_back(p);
        }
   };


   SegmentDatabase Validator::release(){
        //BOOST_LOG_SEV(lg,info) <<"#Validator::release ";


        //release validated segments
        SegmentDatabase ret;

        while(unvalidated.size()>0 and unvalidated.front().segments.size()>0) {
                ready.merge(unvalidated.front().segments);
                unvalidated.pop_front();
                ib++;
                if(ib==batchSize) {
                        ret.merge(ready);
                        ib=0;
                        break;
                }
        }
        //std::cout<<"e "<<unvalidated.size()<<std::endl;
        return ret;
   };

 */







void Validator::init(std::set<int> clients_,int batchSize_){
	clients=clients_;
	batchSize=batchSize_;

	ib=0;
	for(auto it=clients.begin(); it!=clients.end(); it++) {
		Placeholder p;
		p.producerID=*it;
		locations[*it]=unvalidated.insert(unvalidated.end(),p);
		//unvalidated.push_back(p);
		//locations[*it]=unvalidated.rbegin();
	}
};


void Validator::validate(int peer, SegmentDatabase &batch){

	if(batch.size()==0) {
		std::cout<<"Validator::validate: WARNING! Validating an empty batch from "<<peer<<std::endl;
	}

	bool phFound=false;
	if(locations.count(peer)>0  ) {
		phFound=true;
		locations[peer]->segments=batch;
		if(batch.size()==0) {
			unvalidated.erase(locations[peer]);
		}
		locations.erase(peer);
	}


	//placeholder not found
	if(not phFound and batch.size()>0) {
		std::cout<<"Validator::validate: PLACEHOLDER NOT FOUND"<<std::endl;
		//BOOST_LOG_SEV(lg,warning) <<"#Validator::validate placeholder not found";
		//queue at the end
		Placeholder p;
		p.producerID=peer;
		p.segments=batch;
		unvalidated.push_back(p);
	}

	//insert new placeholder at the end
	{
		Placeholder p;
		p.producerID=peer;
		locations[peer]=unvalidated.insert(unvalidated.end(),p);
	}

	int age=0;
	int head=-1;
	if(unvalidated.size()>0) {
		head=unvalidated.front().producerID;
		age=std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-unvalidated.front().queued).count();
	}
	{
		//std::cout<<"#SegmentDatabase Validator::validate : "<<unvalidated.size()<<" items in unvalidated queue, head "<< head <<" is "<<age<<"s old"<<std::endl;
	}

};

void Validator::refreshHead(int timeout){

	int age;
	if(unvalidated.size()>0) {

		//do
		while(true) {
			int head=unvalidated.front().producerID;
			age=std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-unvalidated.front().queued).count();
			if(age>timeout and unvalidated.front().segments.size()==0) {
				std::cout<<"HEAD IS "<<age<<" SECONDS OLD. DEMOTING IT TO THE END OF THE QUEUE"<<std::endl;
				Placeholder p;
				p.producerID=head;
				locations[head]=unvalidated.insert(unvalidated.end(),p);
				unvalidated.pop_front();
			}
			else{
				break;
			}
		} //while(age>timeout and unvalidated.front().segments.size()==0);
	}
};

SegmentDatabase Validator::release(){
	//BOOST_LOG_SEV(lg,info) <<"#Validator::release ";

	//release validated segments
	SegmentDatabase ret;

	while(unvalidated.size()>0 and unvalidated.front().segments.size()>0) {
		ready.merge(unvalidated.front().segments);
		unvalidated.pop_front();
		ib++;
		if(ib==batchSize) {
			ret.merge(ready);
			ib=0;
			break;
		}
	}


	return ret;
};
