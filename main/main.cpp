/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#include <iostream>
#include <algorithm>

#include "Constants.hpp"
#include "XYZSystem.hpp"
#include "LAMMPSSystem.hpp"
#include "LAMMPSEngine.hpp"
#include "DummyEngine.hpp"
#include "Task.hpp"
#include "Graph.hpp"
#include "TaskManager.hpp"
#include "Worker.hpp"

#include <mpi.h>
#include <vector>
#include <ostream>
#include <streambuf>
#include <sstream>
#include <unistd.h>


#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <chrono>
#include <thread>


//This file contains the type definitions. Edit this file to change to LAMMPS classes
#include "CustomTypes.hpp"

#include "HCDS.hpp"
#include "LocalStore.hpp"
#include "DDS.hpp"
#include "Splicer.hpp"
#include "NodeManager.h"
#include "AbstractSystem.hpp"


#include "DDS3.hpp"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

int main(int argc, char * argv[]) {
	std::cout << "ParSplice\n";
	pid_t pid=getpid();
	std::cout<<"PID: "<<pid<<std::endl;

	MPI_Init(&argc, &argv);

	int rank;
	int parent;
	int nranks;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nranks);

	std::cout << "ParSplice: MPI_Init done\n";

	// Create empty property tree object
	boost::property_tree::ptree tree;
	// Parse the XML into the property tree.
	boost::property_tree::read_xml("./input/ps-config.xml", tree,boost::property_tree::xml_parser::no_comments | boost::property_tree::xml_parser::trim_whitespace);
	//boost::property_tree::read_info("./input/ps-config.info", tree);

	std::cout << "ParSplice: XML read\n";



	std::string dbRoot=tree.get<std::string>("ParSplice.DB.RootDirectory","./");


#if 1

	std::ifstream inlayout;
	inlayout.open("layout",std::ios::in);

	std::map<int,std::tuple<int,int,int,std::string,int> > layout;
	std::map<int,int> myChildren;
	std::set<int> groups;

	int r,t,p,seed;
	std::string s;


	while(inlayout>>r>>t>>p>>s>>seed) {
		layout[r]=std::make_tuple(r,t,p,s,seed);
		if(p==rank) {
			if(myChildren.count(t)==0) {
				myChildren[t]=r;
			}
			else{
				myChildren[t]=std::min(r,myChildren[t]);
			}
		}
	}
	inlayout.close();

	int myGroup=std::get<1>(layout[rank]);
	int myParent=std::get<2>(layout[rank]);
	std::string myType=std::get<3>(layout[rank]);
	int mySeed=std::get<4>(layout[rank]);

	std::cout<<"LAYOUT: "<<myGroup<<" "<<myParent<<" "<<myType<<std::endl;
	std::set<int> dbKeys;
	dbKeys.insert(0);
	dbKeys.insert(1);

	std::unordered_map< unsigned int, std::pair<bool,bool> > dbAttributesMin;
	dbAttributesMin[0]=std::make_pair(false,false);
	dbAttributesMin[1]=std::make_pair(false,false);


	MPI_Comm dbComm;
	MPI_Comm workComm;
	MPI_Comm localComm;

	//form local groups
	MPI_Comm_split(MPI_COMM_WORLD, myGroup, 1, &localComm);



	std::chrono::high_resolution_clock::time_point start=std::chrono::high_resolution_clock::now();
	std::chrono::minutes runTime=std::chrono::minutes( tree.get<unsigned>("ParSplice.RunTime",1000000)+1 );

	// Ask Splicer toto generate an input file for every sub-domain (SD)
	// TODO: Definitely want to avoid writing a file for every SD...
  int ntrajectories=1;
  bool useSubLattice=0;
	if(myType=="Splicer") {
    ntrajectories=tree.get<unsigned>("ParSplice.MultiTrajectories.ntrajectories",1);
		useSubLattice=tree.get<unsigned>("ParSplice.MultiTrajectories.useSubLattice",0);
		if(useSubLattice) {
			std::cout<<"Using Sub-lattice Decomposition! (Experimental Version)"<<std::endl;
			int nxsl = tree.get<int>("ParSplice.MultiTrajectories.nxSL",1);
			int nysl = tree.get<int>("ParSplice.MultiTrajectories.nySL",1);
			int nzsl = tree.get<int>("ParSplice.MultiTrajectories.nzSL",1);
			double dsl1 = tree.get<double>("ParSplice.MultiTrajectories.dbufSL",5.0);
			double dsl2 = tree.get<double>("ParSplice.MultiTrajectories.dfixSL",5.0);
			double dslv = tree.get<double>("ParSplice.MultiTrajectories.vacSL",5.0);
			//rjz// Locate and run python script. This will create a seperate input files for each SLD
			std::vector<std::string> pycmd_list;
			std::string SLDpyScript=tree.get<std::string>("ParSplice.MultiTrajectories.SLDpyScript");
			std::string initConfig=tree.get<std::string>("ParSplice.InitialConfiguration");
			boost::trim(SLDpyScript);
			boost::trim(initConfig);
			pycmd_list.push_back("python");
			pycmd_list.push_back(SLDpyScript);
			pycmd_list.push_back(initConfig);
			pycmd_list.push_back(boost::lexical_cast<std::string>(nxsl));
			pycmd_list.push_back(boost::lexical_cast<std::string>(nysl));
			pycmd_list.push_back(boost::lexical_cast<std::string>(nzsl));
			pycmd_list.push_back(boost::lexical_cast<std::string>(dsl1));
			pycmd_list.push_back(boost::lexical_cast<std::string>(dsl2));
			pycmd_list.push_back(boost::lexical_cast<std::string>(dslv));
			std::string py_cmd = boost::algorithm::join(pycmd_list," ");
			std::system(py_cmd.c_str());
		}
	}
	// Everyone waits for Splicer
	MPI_Barrier(MPI_COMM_WORLD);

	uint64_t maxDataSize=100000000;
	uint64_t maxCacheSize=1000000000;

	if(myType=="Splicer") {

		//db-comm
		MPI_Comm_split(MPI_COMM_WORLD,1,2,&dbComm);
		//work comm
		MPI_Comm_split(MPI_COMM_WORLD,1,0,&workComm);


		//std::cout<<"INTRA"<<std::endl;

		//form intercommunicator with child
		MPI_Comm worker;
		assert(myChildren.size()==1);
		for(auto it=myChildren.begin(); it!=myChildren.end(); it++) {
			std::cout<<it->second<<" "<<it->first<<std::endl;
			MPI_Intercomm_create( localComm, 0, MPI_COMM_WORLD, it->second, it->first, &worker);
		}

		//std::cout<<"INTER"<<std::endl;

		std::set<int> children;
		int nrankWork;
		MPI_Comm_size(workComm, &nrankWork);
		for(int i=1; i<nrankWork; i++) {
			std::cout<<"CHILDREN: "<<i<<std::endl;
			children.insert(i);
		}
		HDDS3<STLLocalDataStore> minimaStore(dbComm,1,dbRoot+"./db0/","min",maxDataSize,maxCacheSize,dbKeys,dbAttributesMin,true);
    
    if((useSubLattice) || (ntrajectories > 1)){
      // Use Splicer for SL-ParSplice (for multiple official trajectories):
      SLParSpliceSplicer splicer(workComm,&minimaStore,children,tree,worker);
      std::cout<<"SERVER() CALLED BY.. "<<myType<<std::endl<<std::flush;
      splicer.server();
    }else{
      // Use ParSplice Splicer (for single official trajectory):
      ParSpliceSplicer splicer(workComm,&minimaStore,children,tree,worker);
      std::cout<<"SERVER() CALLED BY.. "<<myType<<std::endl<<std::flush;
      splicer.server();
    }

		std::cout<<"Splicer is done"<<std::endl;
	}

	//std::cout<<"CHECKPOINT 1 myType="<<myType<<std::flush;

	if(myType=="WorkManager") {

		//db-comm
		MPI_Comm_split(MPI_COMM_WORLD,1,2,&dbComm);
		//work comm
		MPI_Comm_split(MPI_COMM_WORLD,1,1,&workComm);

		//std::cout<<"INTRA"<<std::endl;

		//form intercommunicators with children
		std::set<MPI_Comm> workers;
		for(auto it=myChildren.begin(); it!=myChildren.end(); it++) {
			MPI_Comm interComm;
			std::cout<<it->second<<" "<<it->first<<std::endl;
			MPI_Intercomm_create( localComm, 0, MPI_COMM_WORLD, it->second, it->first, &interComm);
			workers.insert(interComm);
		}
		//std::cout<<"INTER"<<std::endl;

		STLLocalDataStore qsdStore;
		qsdStore.initialize("","");
		qsdStore.createDatabase(0, true, true);
		qsdStore.setMaximumSize(1000000000);
		HDDS3<STLLocalDataStore> minimaStore(dbComm,1,dbRoot+"./db0/","min",maxDataSize,maxCacheSize,dbKeys,dbAttributesMin,true);
		WorkManager workManager(workComm, 0, &minimaStore, &qsdStore,  tree, workers);
		workManager.server();
		std::cout<<"WorkManager is done"<<std::endl;
	}

	//std::cout<<"CHECKPOINT 2 myType="<<myType<<std::flush;

	if(myType=="PersistentDB") {

		//db-comm
		MPI_Comm_split(MPI_COMM_WORLD,1,0,&dbComm);
		//work comm
		MPI_Comm_split(MPI_COMM_WORLD,MPI_UNDEFINED,0,&workComm);

		//std::cout<<"INTRA"<<std::endl;

		HDDS3<BDBLocalDataStore> minimaStore(dbComm,-1,dbRoot+"./db0/","min",maxDataSize,maxCacheSize,dbKeys,dbAttributesMin,false);
		std::chrono::seconds syncDelay( tree.get<unsigned>("ParSplice.DB.SyncDelay",1) );

		std::chrono::high_resolution_clock::time_point lastSync=std::chrono::high_resolution_clock::now();
		while(true) {
			minimaStore.singleServe();
			//minimaStore.printStatus();

			if(std::chrono::high_resolution_clock::now() - lastSync > syncDelay  ) {
				std::cout<<"DB SYNC"<<std::endl;
				minimaStore.printStatus();
				minimaStore.sync();
				lastSync=std::chrono::high_resolution_clock::now();
			}

			if(std::chrono::high_resolution_clock::now() - start > runTime  ) {
				std::cout<<"DB is going down"<<std::endl;
				minimaStore.sync();
				minimaStore.cancelCommunications();
				break;
			}

		}
		std::cout<<"PersistentDB is done"<<std::endl;
	}

	//std::cout<<"CHECKPOINT 3 myType="<<myType<<std::flush;

	if(myType=="InMemoryDB") {
		//db-comm
		MPI_Comm_split(MPI_COMM_WORLD,1,1,&dbComm);
		//work comm
		MPI_Comm_split(MPI_COMM_WORLD,MPI_UNDEFINED,0,&workComm);

		//std::cout<<"INTRA"<<std::endl;

		HDDS3<STLLocalDataStore> minimaStore(dbComm,0,dbRoot+"./db0/","min",maxDataSize,maxCacheSize,dbKeys,dbAttributesMin,true);
		std::chrono::seconds syncDelay( tree.get<unsigned>("ParSplice.DB.SyncDelay",1) );

		std::chrono::high_resolution_clock::time_point lastSync=std::chrono::high_resolution_clock::now();
		while(true) {
			minimaStore.singleServe();
			if(std::chrono::high_resolution_clock::now() - lastSync > syncDelay  ) {
				std::cout<<"DB SYNC"<<std::endl;
				minimaStore.printStatus();
				//minimaStore.sync();
				lastSync=std::chrono::high_resolution_clock::now();
			}
			if(std::chrono::high_resolution_clock::now() - start > runTime  ) {
				std::cout<<"DB is going down"<<std::endl;
				minimaStore.sync();
				minimaStore.cancelCommunications();
				break;
			}
		}
		std::cout<<"InMemoryDB is done"<<std::endl;
		//std::cout<<"CHECKPOINT 4 myType="<<myType<<std::flush;

	}

	if(myType=="Worker") {
		//db-comm
		MPI_Comm_split(MPI_COMM_WORLD,MPI_UNDEFINED,0,&dbComm);
		//work comm
		MPI_Comm_split(MPI_COMM_WORLD,MPI_UNDEFINED,0,&workComm);

		int nloc;
		MPI_Comm_size(localComm, &nloc);

		//std::cout<<"INTRA "<<nloc<<std::endl;

		std::cout<<myParent<<" "<<myGroup<<std::endl;

		MPI_Comm interComm;
		MPI_Intercomm_create( localComm, 0, MPI_COMM_WORLD, myParent, myGroup, &interComm);

		//std::cout<<"INTER"<<std::endl;

		worker(localComm,interComm,mySeed);
		std::cout<<"Worker is done"<<std::endl;
	}

	MPI_Finalize();
#endif
}
