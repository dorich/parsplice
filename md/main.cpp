#include <iostream>
#include <algorithm>

#include "Constants.hpp"
#include "XYZSystem.hpp"
#include "LAMMPSSystem.hpp"
#include "LAMMPSEngine.hpp"
#include "DummyEngine.hpp"
#include "Task.hpp"
#include "Graph.hpp"
#include "TaskManager.hpp"
#include "Worker.hpp"

#include <mpi.h>
#include <vector>
#include <ostream>
#include <streambuf>
#include <sstream>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <chrono>
#include <thread>


//This file contains the type definitions. Edit this file to change to LAMMPS classes
#include "CustomTypes.hpp"

#include "HCDS.hpp"
#include "LocalStore.hpp"
#include "DDS.hpp"
#include "Splicer.hpp"
#include "NodeManager.h"
#include "AbstractSystem.hpp"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>



int main(int argc, char * argv[]) {
	MPI_Init(&argc, &argv);
	int rank;
	int nranks;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nranks);


	std::cout << "ParSplice-md\n";

	// Create empty property tree object
	boost::property_tree::ptree config;
	// Parse the XML into the property tree.
	boost::property_tree::read_xml("./input/ps-config.xml", config, boost::property_tree::xml_parser::no_comments );
	//boost::property_tree::read_info("./input/ps-config.info", tree);


	std::map< std::pair<int,int>, std::map<std::string,std::string> > parameters;
	//read task parameters
	BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.TaskParameters")) {
		std::string stype=v.second.get<std::string>("Type");
		boost::trim(stype);
		int type=taskTypeByLabel.at(stype);
		int flavor=v.second.get<int>("Flavor");
		BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("")) {
			std::string key=vv.first;
			std::string data=vv.second.data();
			boost::trim(key);
			boost::trim(data);
			std::cout<<key<<" "<<data<<std::endl;
			parameters[std::make_pair(type,flavor)][key]=data;
		}
	}


	MPI_Comm localComm, workerComm;

	if(rank==0) {

		MPI_Comm_split(MPI_COMM_WORLD, 0, 0, &localComm);

		MPI_Intercomm_create( localComm, 0, MPI_COMM_WORLD, 1, 1, &workerComm);


		DriverHandleType handle(workerComm);

		std::ofstream outTraj;
		outTraj.open("./traj.out", std::ios::app);

		TaskType task;
		std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
		boost::trim(initialConfiguration);

		task.type=PARSPLICE_TASK_INIT_FROM_FILE;
		task.parameters=parameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,1)];
		task.parameters["Filename"]=initialConfiguration;
		handle.assign(task);
		while(not handle.probe(task)) {};
		task.parameters.clear();

    int totalduration=0;
    int stopduration=config.get<unsigned>("ParSplice.StopDuration",1000000);
		int ic=0;
		Label previous;
		Label current;

		task.type=PARSPLICE_TASK_WRITE_TO_FILE;
		task.parameters=parameters[std::make_pair(PARSPLICE_TASK_WRITE_TO_FILE,1)];
		task.parameters["Label"]=boost::str(boost::format("%1%" ) % ic );
		handle.assign(task);
		while(not handle.probe(task)) {};
		ic++;

		while(true) {
			task.type=PARSPLICE_TASK_SEGMENT;
			task.parameters=parameters[std::make_pair(PARSPLICE_TASK_SEGMENT,1)];
			handle.assign(task);
			while(not handle.probe(task)) {};

			//std::cout<<"TRAJECTORY"<<std::endl;
			std::stringstream ss;
			for(auto it=task.trajectory.visits.begin(); it!=task.trajectory.visits.end(); it++) {
				ss<<" "<<it->label<<" "<<it->duration<<"\n";
        totalduration+=it->duration;
				//std::cout<<it->label<<" "<<it->duration<<"\n";
			}
			outTraj<<ss.str();
			outTraj.flush();

			SystemType s1=task.systems[task.systems.size()-2];
			SystemType s2=task.systems[task.systems.size()-1];
			task.systems.clear();
			task.systems.push_back(s1);
			task.systems.push_back(s2);

			/*
			   std::cout<<"===="<<std::endl;
			   for(int i=0; i<task.systems.size(); i++) {
			        std::cout<<task.systems[i].label<<std::endl;
			   }
			 */

			if(task.trajectory.visits.begin()->label!=task.trajectory.visits.rbegin()->label) {
				task.type=PARSPLICE_TASK_WRITE_TO_FILE;
				task.parameters=parameters[std::make_pair(PARSPLICE_TASK_WRITE_TO_FILE,1)];
				task.parameters["Label"]=boost::str(boost::format("%1%" ) % ic );
				handle.assign(task);
				while(not handle.probe(task)) {};
				ic++;
			}

			/*
			   std::cout<<"===="<<std::endl;
			   for(int i=0; i<task.systems.size(); i++) {
			        std::cout<<task.systems[i].label<<std::endl;
			   }
			 */

      if(totalduration >= stopduration){
  			task.type=PARSPLICE_TASK_DIE;
  			handle.assign(task);
        break;
      }

		}
	}
	else{
		MPI_Comm_split(MPI_COMM_WORLD, 1, 0, &localComm);
		MPI_Intercomm_create( localComm, 0, MPI_COMM_WORLD, 0, 1, &workerComm);
		worker(localComm,workerComm,1234);
	}


  std::cout<<"FINISHING, RANK="<<rank<<std::endl;
  MPI_Barrier(MPI_COMM_WORLD);
 	MPI_Finalize();
	return 0;

};
